package test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.ArrayList;
import java.util.List;


public class DeleteProvisioningAccounts {
    public DeleteProvisioningAccounts() {
        super();
    }
    private Connection getOIM11gDBConnection() {
        Connection oimDBConnection = null;
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            oimDBConnection =
                    DriverManager.getConnection("jdbc:oracle:thin:@oralcdcddb03.nyumc.org:1521/oimdev",
                                                "r2am_oim", "nyuL4ng0n3");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return oimDBConnection;
    }

    public void deleteProvisioningStatus() throws Exception{
        Connection oimDBConnection = getOIM11gDBConnection();

        try {

        List<String> orcKeyList = findORCKEYForProvisioning(oimDBConnection);

        for(String orcKey:orcKeyList) {
            System.out.println("orc_Key = "+orcKey);
            executeDeleteQuery("delete from ent_assign where OIU_key in (select oiu_key from oiu where orc_key = ?)", orcKey, oimDBConnection);
            executeDeleteQuery("delete from oud where OIU_key in (select oiu_key from oiu where orc_key = ?)", orcKey, oimDBConnection);

                executeDeleteQuery("delete from oiu where orc_key = ?", orcKey, oimDBConnection);
                executeDeleteQuery("delete from osi where orc_key = ?", orcKey, oimDBConnection);
                executeDeleteQuery("delete from oti where orc_key = ?", orcKey, oimDBConnection);
            executeDeleteQuery("delete from RECON_ACCOUNT_MATCH where orc_key = ?", orcKey, oimDBConnection);

                executeDeleteQuery("delete from orc where orc_key = ?", orcKey, oimDBConnection);
            executeDeleteQuery("delete from UD_EX_CH where orc_key = ?", orcKey, oimDBConnection);
            executeDeleteQuery("delete from UD_exchange where orc_key = ?", orcKey, oimDBConnection);
            executeDeleteQuery("delete from orc where orc_key = ?", orcKey, oimDBConnection);

        }
        } catch(Exception e){
            e.printStackTrace();
            try {
                    if (oimDBConnection != null)
                            oimDBConnection.close();
            } catch (Exception e1) {
                e1.printStackTrace();
            }

            } finally {
                try {
                        if (oimDBConnection != null)
                                oimDBConnection.close();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }

            }
    }
    public void executeDeleteQuery(String query, String key, Connection con) throws Exception{
            PreparedStatement stmt = null;
            try {
                    stmt = con.prepareStatement(query);
                    stmt.setString(1, key);
                    stmt.executeUpdate();
            } catch (Exception e1) {
                System.out.println("Failed query " + query + " = " + key );
                throw e1;
            } finally {
                    try {
                            if (stmt != null)
                                    stmt.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
            }
    }
    /**
     * It closes database connections.
     * @param rs
     * @param stmt
     * @param connection
     */
    public static void closeConnections(ResultSet rs, Statement stmt, Connection connection){
    if (rs != null) {
        try {
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    if (stmt != null) {
        try {
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    if (connection != null) {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    }

    private List<String> findORCKEYForProvisioning(Connection conn) {
    String query = "select oiu.orc_key from oiu, ost, ud_exchange, usr where oiu.ost_key = ost.ost_key and ost.ost_status = 'Provisioning' and ud_exchange.orc_key=oiu.orc_key and oiu.usr_key=usr.usr_key and usr.USR_CREATE>'31-AUG-2015'";
    Statement stmt = null;
    List<String> orcKeyList = new ArrayList<String>();
    ResultSet rs = null;

    try {
        stmt = conn.createStatement();

        rs = stmt.executeQuery(query);
        while (rs.next()) {
            orcKeyList.add(rs.getString("orc_key"));
        }

    } catch (Exception e1) {
        e1.printStackTrace();
    } finally {
            try {
            if (rs != null)
                rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (stmt != null)
                stmt.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    return orcKeyList;
    }

    public static void main(String[] args) throws Exception {
        DeleteProvisioningAccounts del=new DeleteProvisioningAccounts();
        del.deleteProvisioningStatus();
    }
}
