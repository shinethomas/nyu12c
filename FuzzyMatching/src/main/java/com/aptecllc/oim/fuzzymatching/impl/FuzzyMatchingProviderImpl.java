package com.aptecllc.oim.fuzzymatching.impl;

//~--- non-JDK imports --------------------------------------------------------

//~--- JDK imports ------------------------------------------------------------
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.sql.DataSource;

import oracle.iam.identity.exception.UserSearchException;
import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.platform.OIMClient;
import oracle.iam.platform.authz.exception.AccessDeniedException;
import oracle.iam.platform.entitymgr.vo.SearchCriteria;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aptecllc.oim.fuzzymatching.config.CriteriaType;
import com.aptecllc.oim.fuzzymatching.config.MatchRuleType;
import com.aptecllc.oim.fuzzymatching.config.ParameterType;
import com.aptecllc.oim.fuzzymatching.config.SourceConfigurationType;
import com.aptecllc.oim.matching.api.MatchResult;
import com.aptecllc.oim.matching.api.Matcher;
import com.aptecllc.oim.matching.api.MatchingProvider;
import com.aptecllc.oim.matching.services.MatchServices;
import com.aptecllc.oim.matching.services.PartialMatchDAO;
import com.aptecllc.oim.matching.vo.PartialMatch;
import com.aptecllc.oim.utils.OimUserUtils;
import com.thortech.xl.crypto.*;

/**
 * Fuzzy Matching algorithm modifies the OIM user with Source Attributes for a
 * match.
 * 
 * @author Asif.Syed@aptecllc.com, eric.fisher@aptecllc.com
 * 
 */
public class FuzzyMatchingProviderImpl implements MatchingProvider {

	/**
	 * Variables for logging
	 */
	private final static Logger LOGGER = LoggerFactory
			.getLogger(FuzzyMatchingProviderImpl.class);
	private String jndiMatchDB = "";
	private List<ProcessRule> matchingRules = new ArrayList<ProcessRule>();
	protected PartialMatchDAO matchDAO = null;
	private String returnAttributes = "";
	private OimUserUtils oimUserUtils = null;
	private boolean isSimulateMode = false;
	private SourceConfigurationType sourceConfiguration;
	protected OIMClient client;
	private String resourceName;

	/**
	 * Constructs ...
	 * 
	 */
	public FuzzyMatchingProviderImpl() {
	}

	/**
	 * Constructs ...
	 * 
	 * 
	 * @param config
	 */
	public FuzzyMatchingProviderImpl(SourceConfigurationType config,
			String jndiMatchDB, String resourceName) {
		this.sourceConfiguration = config;
		matchDAO = new PartialMatchDAO();
		this.returnAttributes = buildReturnAttrList();
		this.isSimulateMode = config.isSimulateMode();
		this.jndiMatchDB = jndiMatchDB;
		this.resourceName = resourceName;
		try {
			matchDAO.setDataSource(jndiMatchDB);
		} catch (Exception e) {
			LOGGER.error("Error Initializing DB", e);
		}

		loadMatchingRules();
		oimUserUtils = new OimUserUtils();
	}

	/**
	 * Constructs ...
	 * 
	 * 
	 * @param config
	 * @param ds
	 */
	public FuzzyMatchingProviderImpl(SourceConfigurationType config,
			DataSource ds) {
		this.sourceConfiguration = config;
		matchDAO = new PartialMatchDAO();
		this.returnAttributes = buildReturnAttrList();
		this.isSimulateMode = config.isSimulateMode();
		this.jndiMatchDB = jndiMatchDB;

		try {
			matchDAO.setDataSource(ds);
		} catch (Exception e) {
			LOGGER.error("Error Initializing DB", e);
		}

		loadMatchingRules();
		oimUserUtils = new OimUserUtils();
	}

	/**
	 * Constructs ...
	 * 
	 * 
	 * @param client
	 * @param config
	 */
	public FuzzyMatchingProviderImpl(OIMClient client,
			SourceConfigurationType config) {
		this.client = client;
		this.sourceConfiguration = config;
		this.isSimulateMode = config.isSimulateMode();
		loadMatchingRules();
		this.returnAttributes = buildReturnAttrList();
		matchDAO = new PartialMatchDAO();

		try {
			matchDAO.setDataSource(jndiMatchDB);
		} catch (Exception e) {
			LOGGER.error("Error Initializing DB", e);
		}

		if (client != null) {
			oimUserUtils = new OimUserUtils(client);
		} else {
			oimUserUtils = new OimUserUtils();
		}
	}

	/**
	 * Method description
	 * 
	 * 
	 * @param reconKey
	 * @param sourceDataSet
	 * 
	 * @return
	 * 
	 * @throws Exception
	 */
	@Override
	public MatchResult match(Long reconKey,
			HashMap<String, Object> sourceDataSet, String resourceName)
			throws Exception {
		MatchResult result = null;
		HashMap<String, Object> candidates = null;
		PartialMatch partialMatch = new PartialMatch();
		HashMap<String, Integer> scores = new HashMap<String, Integer>();
		List<String> fullMatches = new ArrayList<String>();
		List<String> partialMatches = new ArrayList<String>();

		// First check to see if this event is already in a partial review
		// queue.

		// Second verify no exact match from previous operations
		String exactID = checkForExactMatch(sourceDataSet
				.get(sourceConfiguration.getSourceKeyAttribute()));

		if (exactID != null) {
			LOGGER.debug("Exact Match by UniqueID Found");
			fullMatches.add(exactID);
		} else {

			// Get Candidate Matches
			candidates = findCandidates(sourceDataSet);

			if ((candidates == null) || candidates.isEmpty()) {
				result = MatchResult.NO_MATCH;
			}

			// Dispatch Candidates to Rules
			partialMatch
					.setSourceResource(sourceConfiguration.getMatchSource());
			partialMatch.setSourceData(sourceDataSet);
			partialMatch.setSourceID((String) sourceDataSet
					.get(sourceConfiguration.getSourceKeyAttribute()));
			partialMatch.setReconKey(reconKey);

			if (result == null) {
				for (String entity : candidates.keySet()) {
					Integer score = scoreCandidate(sourceDataSet,
							(HashMap<String, Object>) candidates.get(entity));

					scores.put(entity, score);

					if (score >= sourceConfiguration.getMinFullMatchPoints()) {
						fullMatches.add(entity);
					} else if (score >= sourceConfiguration
							.getMinPartialMatchPoints()) {
						partialMatches.add(entity);
					}
				}
			}
		}

		// Process Match Results
		if (fullMatches.size() == 1) {

			// Single Full Match - YAY!!
			String usrKey = fullMatches.get(0);

			LOGGER.info("One Full Match Found: " + usrKey);

			if (!isSimulateMode) {
				MatchServices.linkUser(reconKey, Long.parseLong(usrKey),
						resourceName);
			} else {
				LOGGER.info("Simulating: Link User to: " + usrKey);
			}

			result = MatchResult.ONE_MATCH;
		} else if (fullMatches.size() > 1) {

			// More than one full match - BOO!!
			for (String entityID : fullMatches) {

				// Add to candidates
				partialMatch.getCandidates().add(
						partialMatch.buildCandidate(entityID, scores
								.get(entityID),
								(HashMap<String, Object>) candidates
										.get(entityID)));
			}

			if (handlePartialMatch(partialMatch)) {
				if (!isSimulateMode) {
					System.out.println("**Partial Match - Close Event**");
					//MatchServices.closeEvent(reconKey);
					System.out.println("**Partial Match - Close Event code updated 02/01/2017**");
				} else {
					LOGGER.info("Simulating: Partial Match - Close Event");
				}
			}
			

			result = MatchResult.MULTI_MATCH;
		} else if (partialMatches.size() > 0) {

			// More than one full match - BOO!!
			for (String entityID : partialMatches) {

				// Add to candidates
				partialMatch.getCandidates().add(
						partialMatch.buildCandidate(entityID, scores
								.get(entityID),
								(HashMap<String, Object>) candidates
										.get(entityID)));
			}

			if (handlePartialMatch(partialMatch)) {
				if (!isSimulateMode) {
					// Trying to not close the event - makes it easier for
					// resolution
					// MatchServices.closeEvent(reconKey);
				} else {
					LOGGER.info("Simulating: Partial Match - Close Event");
				}
			}

			result = MatchResult.MULTI_MATCH;
		} else {

			// No Match
			LOGGER.info("No Match Found");

			if (!isSimulateMode) {
				MatchServices.createNewUser(reconKey, resourceName);
			} else {
				LOGGER.info("Simulating: Create as New User");
			}

			result = MatchResult.NO_MATCH;
		}

		return result;
	}

	/**
	 * Method description
	 * 
	 * 
	 * @param sourceDataSet
	 * 
	 * @return
	 * 
	 * @throws Exception
	 */
	@Override
	public MatchResult match(HashMap<String, Object> sourceDataSet,
			String resourceName) throws Exception {
		Object reKey = sourceDataSet.get("ReconEventKey");

		return match((Long) reKey, sourceDataSet, resourceName);
	}

	/**
	 * Method description
	 * 
	 * 
	 * @param sourceKey
	 * 
	 * @return
	 */
	protected String checkForExactMatch(Object sourceKey) {
		String returnVal = null;
		SearchCriteria exactCriteria = new SearchCriteria(
				sourceConfiguration.getOIMKeyAttribute(), sourceKey,
				SearchCriteria.Operator.EQUAL);

		// Do the Search
		List<User> results = new ArrayList<User>();

		try {
			results = oimUserUtils.getUser(exactCriteria, null);
		} catch (UserSearchException e) {
			LOGGER.error(e.getMessage(), e);
		} catch (AccessDeniedException e) {
			LOGGER.error(e.getMessage(), e);
		}

		LOGGER.debug("User Search Results: " + results.size());

		if (results.size() == 1) {

			// One exact rule match found.
			returnVal = results.get(0).getEntityId();
		}

		return returnVal;
	}

	/**
	 * Method description
	 * 
	 * 
	 * @param sourceDataSet
	 * 
	 * @return
	 */
	protected HashMap<String, Object> findCandidates(
			HashMap<String, Object> sourceDataSet) {
		System.out.println("Entering in findCandidates with sourceDataSet " + sourceDataSet);
		
		HashMap<String, Object> candidateMap = new HashMap<String, Object>();
		List<CriteriaType> criteria = sourceConfiguration
				.getCandidateSelection().getSelectionCriteria().getCriteria();
		System.out.println("criteria size is:"+criteria.size());
		SearchCriteria oimCriteria = null;

		for (CriteriaType criterion : criteria) {
			System.out.println("In For Loop criterion " + criterion);
			System.out.println("In For Loop criterion.getSourceAttribute() " + criterion.getSourceAttribute());

			
			if (sourceDataSet.get(criterion.getSourceAttribute()) != null) {
				SearchCriteria oimCriterion = new SearchCriteria(
						criterion.getCandidateAttribute(),
						sourceDataSet.get(criterion.getSourceAttribute()),
						SearchCriteria.Operator.valueOf(criterion.getOperator()));
				System.out.println("oimCriterion"+oimCriterion);

				if (oimCriteria == null) {
					oimCriteria = oimCriterion;
				} else {

					// Combine
					SearchCriteria combined = new SearchCriteria(oimCriteria,
							oimCriterion, SearchCriteria.Operator.OR);

					oimCriteria = combined;
				}
			}
		}
		//oimCriteria = new SearchCriteria("Last Name",fieldvalue, SearchCriteria.Operator.EQUAL);
        System.out.println("--------Candidate criteria------------"+oimCriteria);
		//LOGGER.debug("Candidate Criteria: " + oimCriteria.toString());

		// Do the Search
		List<User> results = new ArrayList<User>();

		try {
			results = oimUserUtils.getUser(oimCriteria,
					returnAttributes.toString());
		} catch (UserSearchException e) {
			LOGGER.error(e.getMessage(), e);
		} catch (AccessDeniedException e) {
			LOGGER.error(e.getMessage(), e);
		}

		LOGGER.debug("User Search Results: " + results.size());

		for (User user : results) {
			candidateMap.put(user.getId(), user.getAttributes());
		}

		LOGGER.trace(candidateMap.toString());

		return candidateMap;
	}

	/**
	 * Method description
	 * 
	 * 
	 * @param sourceDataSet
	 * @param candidateDataSet
	 * 
	 * @return
	 */
	protected Integer scoreCandidate(HashMap<String, Object> sourceDataSet,
			HashMap<String, Object> candidateDataSet) {
		Integer score = 0;
        System.out.println("candidateDataSet"+candidateDataSet);
		for (ProcessRule rule : matchingRules) {

			Object sourceValue = extractValue(sourceDataSet, rule
					.getMatchRule().getSourceAttribute());
			System.out.println("sourceValue is ---"+sourceValue);
			Object candidateValue = extractValue(candidateDataSet, rule
					.getMatchRule().getCandidateAttribute());
			System.out.println("candidateValue is ---"+candidateValue);
			//System.out.println("candidateValue is ---"+extractValue(sourceDataSet, rule.getMatchRule().));
			
			//added by Shine Thomas on Oct 20
			if(candidateValue!=null && candidateValue.toString().endsWith("=="))
			{
				System.out.println("This is certainly candidate SSN in hashed format");
				try {
					candidateValue = tcCryptoUtil.decrypt(candidateValue.toString(),"DBSecretKey" );
				} catch (tcCryptoException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//System.out.println("decrypted candidate value is:"+candidateValue);
			}
			
			if(sourceValue!=null && sourceValue.toString().endsWith("=="))
			{
				System.out.println("This is certainly source SSN in hashed format");
				try {
					sourceValue = tcCryptoUtil.decrypt(sourceValue.toString(),"DBSecretKey" );
				} catch (tcCryptoException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//System.out.println("decrypted source Value is:"+sourceValue);
			}
			// code snippet ended
			int performMatchScore = rule.matcher.performMatch(sourceValue,
					candidateValue);
			System.out
					.println("rule.matcher.performMatch(sourceValue, candidateValue):"+rule.matcher.getClass()+" :source value:"+sourceValue+" candidate value:"+candidateValue
							+ " :"+performMatchScore);
			//rule.matcher.performMatch(sourceValue, candidateValue):class com.aptecllc.oim.matching.rule.StringEqualityMatcher :source value:Vedder candidate value:Vedder :30
			score += performMatchScore;
		}
		System.out.println("Computed Score: " + score);
		LOGGER.debug("Computed Score: " + score);

		return score;
	}

	private Object extractValue(HashMap<String, Object> dataSet,
			String attribute) {
		if (attribute.contains(":")) {
			String[] strings = attribute.split(":");
			String value = "";
			for (String string : strings) {
				if (!string.isEmpty())
					value += string + " ";
			}
			System.out.println("value=" + value);
			return value.trim();
		}
		return dataSet.get(attribute);
	}

	// Loops through all of the attributes listed in the rules and adds them to
	// the return list for candidates
	// so that they can be matched.
	private String buildReturnAttrList() {
		StringBuffer buf = new StringBuffer();

		buf.append(sourceConfiguration.getOIMKeyAttribute()).append(",");

		for (MatchRuleType rule : sourceConfiguration.getMatchRule()) {
			buf.append(rule.getCandidateAttribute()).append(",");
		}

		buf.append("usr_key");
		LOGGER.debug("Return Attribute List: " + buf.toString());

		return buf.toString();
	}

	private boolean handlePartialMatch(PartialMatch match) {

		// Check for pending to update first
		Long matchID = null;
		boolean result = false;

		matchID = matchDAO.checkForPendingResolution(match.getSourceResource(),
				match.getSourceID());

		if (matchID != null) {
			LOGGER.debug("Found existing pending match: " + matchID);
			match.setMatchID(matchID);
			result = matchDAO.updatePartialMatch(match);
		} else {
			matchID = matchDAO.getMatchID();
			match.setMatchID(matchID);
			result = matchDAO.insertPartialMatch(match);
		}

		return result;
	}

	private void loadMatchingRules() {

		// Load Matching Rules
		for (MatchRuleType rule : sourceConfiguration.getMatchRule()) {
			String clazz = rule.getMatcher().getImplementationClass();
			HashMap<String, String> params = new HashMap<String, String>();

			for (ParameterType param : rule.getMatcher().getMatcherParameters()) {
				params.put(param.getKey(), param.getValue());
			}

			Matcher matcher = null;

			try {
				matcher = (Matcher) Class.forName(clazz).newInstance();
				matcher.initializeMatcher(params);
			} catch (InstantiationException e) {
				LOGGER.error(e.getMessage(), e);
			} catch (IllegalAccessException e) {
				LOGGER.error(e.getMessage(), e);
			} catch (ClassNotFoundException e) {
				LOGGER.error(e.getMessage(), e);
			}

			matchingRules.add(new ProcessRule(rule, matcher));
		}
	}

	private class ProcessRule {
		private final MatchRuleType matchRule;
		private final Matcher matcher;

		/**
		 * Constructs ...
		 * 
		 * 
		 * @param pMatchRule
		 * @param pMatcher
		 */
		public ProcessRule(MatchRuleType pMatchRule, Matcher pMatcher) {
			matchRule = pMatchRule;
			matcher = pMatcher;
		}

		/**
		 * Method description
		 * 
		 * 
		 * @return
		 */
		public MatchRuleType getMatchRule() {
			return matchRule;
		}

		/**
		 * Method description
		 * 
		 * 
		 * @return
		 */
		public Matcher getMatcher() {
			return matcher;
		}
	}
}
