package com.aptecllc.oim.fuzzymatching.impl;






public class LevenshteinsDistance  {

	public static int LD(String s, String t) throws Exception {
		int n = s.length();
		int m = t.length();

		if (n == 0)
			return m;
		if (m == 0)
			return n;

		int[][] d = new int[n + 1][m + 1];

		for (int i = 0; i <= n; d[i][0] = i++);
		for (int j = 1; j <= m; d[0][j] = j++);

		for (int i = 1; i <= n; i++) {
			char sc = s.charAt(i - 1);
			for (int j = 1; j <= m; j++) {
				int v = d[i - 1][j - 1];
				if (t.charAt(j - 1) != sc)
					v++;
				d[i][j] = Math.min(Math.min(d[i - 1][j] + 1, d[i][j - 1] + 1),
						v);
			}
		}
		return d[n][m];
	}

	/**
	 * 
	 * @param s
	 * @param t
	 * @return
	 */
	public static int nameDistance(String s, String t) throws Exception {
		String[] sa = s.replace("-", "").replace(".","").split(" ");
		String[] ta = t.replace("-", "").replace(".","").split(" ");

		int ttl = 0;
		for (int i = 0; i < sa.length; i++) {
			int st = Integer.MAX_VALUE;
			
			for (int j = 0; j < ta.length; j++) {
				// look for LD() below in my previous tip
				st = Math.min(st, LD(sa[i], ta[j]));
			}
			ttl += st;
		}
		return ttl;
	}
	
	/**
	 * 
	 * @param sourceFullName
	 * @param oimFullName
	 * @param totalWeight
	 * @return
	 * @throws Exception 
	 */
	public static int LDPoints(String sourceFullName, String oimFullName, int totalWeight) throws Exception{
		
		String srcFname = sourceFullName.toUpperCase();
		String oimFname = oimFullName.toUpperCase();
		
		int LD = LevenshteinsDistance.nameDistance(srcFname, oimFname);
		float temp = (float) (srcFname.length() + oimFname.length()) / 2;
		float percent = 1 - ((LD / temp));
		int score = (int) (percent * totalWeight);
		return score;
	}
	
	/**
	 * 
	 * @param str1 First String 
	 * @param str2 Second String
	 * @param weight Preassigned Standard weight for each attribute
	 * @param thresholdPercent Preassigned Standard threshold Percentage it is now 25 i.e 25%
	 * @return LD score
	 * @throws Exception 
	 */
	public static int LDFloor (String str1, String str2,int weight, int thresholdPercent) throws Exception{
		int score = 0;
		float avgLength = (float)(str1.toUpperCase().length() + str2.toUpperCase().length())/2;
		String sourceName = str1.toUpperCase();
		String oimName = str2.toUpperCase();
		int LD = LevenshteinsDistance.nameDistance(sourceName, oimName);

		
		//System.out.println("IndLD: " +LD);
		float percent = (float)thresholdPercent/100;
		float slab = percent * avgLength ;
		//System.out.println("SLAB+ " +slab);
		if (LD > (int)slab){
			score = 0;
		} else {
			float perc = 1 - ((LD/avgLength));
			score = (int)(perc * weight);
		}
		return score;
	}
	

	public static void main(String[] args) throws Exception {
		/*String source = "Paige";
		//System.out.println("Source Length: " +source.length());
		
	
	 * SRC First Name Dakota
SRC Middle Name Name null
SRC Last Name Abrams
OIM First Name DAKOTA
OIM Middle Name Name TYLER
OIM Last Name ABRAMS
	 
		
		String oim = "Leigh"; 
		System.out.println("New Score: " + LDFloor(source, oim, 10, 25));
		//System.out.println("OIM Length: " + oim.length());
		
	
		System.out.println("Traditional Score : " +LDPoints(source, oim,10));*/
		String source_ssn="421558686";
		String oim_ssn = "421558686";
		System.out.println(nameDistance(source_ssn,oim_ssn));
	}
}
