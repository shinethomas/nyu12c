package com.aptecllc.oim.matching.rule;

//~--- non-JDK imports --------------------------------------------------------

import com.aptecllc.oim.matching.api.AbstractMatcher;
import com.aptecllc.oim.matching.api.Matcher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//~--- JDK imports ------------------------------------------------------------

import java.util.Map;

/**
 * Class description
 *
 *
 * @version        1.0, 23.Feb 2015
 * @author         Eric A. Fisher, APTEC LLC    
 */
public class StringEqualityMatcher extends AbstractMatcher implements Matcher {
    private static final Logger LOGGER           = LoggerFactory.getLogger(StringEqualityMatcher.class);
    private boolean             isNullValidMatch = false;

    /**
     * Constructs ...
     *
     */
    public StringEqualityMatcher() {}

    /**
     * Method description
     *
     *
     * @param parameterMap
     */
    public void initializeMatcher(Map parameterMap) {
        super.initializeMatcher(parameterMap);

        String nullValid = this.matcherParams.get("NullMatchValid");

        if ("true".equalsIgnoreCase(nullValid)) {
            isNullValidMatch = true;
        }
    }

    /**
     * Method description
     *
     *
     * @param sourceAttribute
     * @param candidateAttribute
     *
     * @return
     */
    @Override
    public int performMatch(Object sourceAttribute, Object candidateAttribute) {
        int    score     = 0;
        String source    = (String) sourceAttribute;
        String candidate = (String) candidateAttribute;
        String nullValid = this.matcherParams.get("NullMatchValid");
        boolean isNYUNullValidMatch = false;
        
        if ((source == null) && (candidate == null)) {
            //score = this.fullMatchPoints;
        	score = this.noMatchPoints;
            System.out.println("source is null, candidate is null and isNYUNullValidMatch is Null");
        } else {
            if (candidate == null) {
                candidate = "";
                System.out.println("candidate is null");
            }

            if (source == null) {
                source = "";
                System.out.println("source is null");
            }

            source    = source.trim();
            candidate = candidate.trim();
            LOGGER.debug("Source: " + source + " Candidate: " + candidate);
            
            //added by shine on 10/27
            if (source.length()<1 && candidate.length()<1) {

                // This would also catch empty string == empty string
            	System.out.println("This would also catch empty string == empty string");
                score = this.noMatchPoints;
                System.out.println("score = this.noMatchPoints");
            }
            //ended by shine on 10/27
            
            if (source.equalsIgnoreCase(candidate) && source.length()>0 && candidate.length()>0) {

                // This would also catch empty string == empty string
            	System.out.println("This would not catch empty string == empty string");
                score = this.fullMatchPoints;
                System.out.println("score = this.fullMatchPoints");
            } else {
                if (("".equals(source)) || ("".equals(candidate))) {

                    // If either but not both are empty string then match is
                    // indeterminate
                    score = this.noMatchPoints;
                    System.out.println(" Line 94");
                } else {

                    // Otherwise its a no match
                    score = this.noMatchPoints;
                    System.out.println(" Line 99");
                }
            }
        }
        LOGGER.debug("Matcher Score:" + score);
        System.out.println("Matcher Score:" + score);
        return score;
    }
    public static void main(String args[])
    {
    	int    score     = 0;
        String source    = null;
        String candidate = null;
        boolean isNullValidMatch=false;

        if ((source == null) && (candidate == null) && isNullValidMatch) {
            System.out.println("source candidate and isnullValidatch is null");
        }
        else
        {
        	System.out.println("else");
        }
    }
}
