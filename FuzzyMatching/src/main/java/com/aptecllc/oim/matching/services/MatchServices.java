package com.aptecllc.oim.matching.services;

//~--- non-JDK imports --------------------------------------------------------

import oracle.iam.platform.Platform;
import oracle.iam.reconciliation.api.EventMgmtService;
import oracle.iam.reconciliation.api.ReconOperationsService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import Thor.API.Exceptions.tcAPIException;

/**
 * Class description
 * 
 * 
 * @version 1.0, 04.Feb 2015
 * @author Eric A. Fisher, APTEC LLC
 */
public class MatchServices {
	private static final Logger logger = LoggerFactory
			.getLogger(MatchServices.class);

	/**
	 * Method description
	 * 
	 * 
	 * @param rekey
	 * 
	 * @return
	 */
	public static boolean closeEvent(long rekey) {
		ReconOperationsService reconService = Platform
				.getService(ReconOperationsService.class);

		try {
			reconService.closeReconciliationEvent(rekey);

			return true;
		} catch (tcAPIException e) {
			logger.error(e.getMessage(), e);
		}

		return false;
	}

	/**
	 * Method description
	 * 
	 * 
	 * @param rekey
	 * @param resourceName
	 * 
	 * @return
	 */
	public static boolean createNewUser(long rekey, String resourceName) {
		EventMgmtService eventService = Platform
				.getService(EventMgmtService.class);

		try {
			eventService.createUser((Long) rekey, null, resourceName,
					"Created as New");

			return true;
		} catch (Exception e) {
			logger.error("Link Exception" + e.getMessage());
		}

		return false;
	}

	/**
	 * Method description
	 * 
	 * 
	 * @param reKey
	 * @param usrKey
	 * @param resourceName
	 * 
	 * @return
	 */
	public static boolean linkUser(long reKey, long usrKey, String resourceName) {
		EventMgmtService eventService = Platform
				.getService(EventMgmtService.class);

		try {
			eventService.establishUserLink(reKey, usrKey, null, resourceName,
					"Matched to Existing");

			return true;
		} catch (Exception e) {
			logger.error("Link Exception" + e.getMessage());
		}

		return false;
	}
}
