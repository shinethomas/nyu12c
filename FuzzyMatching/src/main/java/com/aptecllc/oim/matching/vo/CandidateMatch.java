package com.aptecllc.oim.matching.vo;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;

public class CandidateMatch {
	private Long matchID;
	private String candidateEntityID;
	private Integer matchScore;
	private HashMap<String, Object> candidateData;
	public Long getMatchID() {
		return matchID;
	}
	public void setMatchID(Long matchID) {
		this.matchID = matchID;
	}
	public String getCandidateEntityID() {
		return candidateEntityID;
	}
	public void setCandidateEntityID(String candidateEntityID) {
		this.candidateEntityID = candidateEntityID;
	}
	public Integer getMatchScore() {
		return matchScore;
	}
	public void setMatchScore(Integer matchScore) {
		this.matchScore = matchScore;
	}
	public HashMap<String, Object> getCandidateData() {
		return candidateData;
	}
	public void setCandidateData(HashMap<String, Object> candidateData) {
		this.candidateData = candidateData;
	}
	
	public byte[] getCandidateDataBlob() {
    	try {
			return getSerializedObject(this.candidateData);
		} catch (Exception e) {
			return null;
		}
    }
    
    private byte[] getSerializedObject(Serializable obj) throws Exception{
    	ByteArrayOutputStream bos = new ByteArrayOutputStream();
    	ObjectOutput out = null;
    	byte[] outBytes = null;
    	
    	
    	  out = new ObjectOutputStream(bos);   
    	  out.writeObject(obj);
    	  outBytes = bos.toByteArray();
    	  if (out != null) {
    	      out.close();
    	    }
    	  bos.close();
    	
    	
    	return outBytes;
    }
}
