package com.aptecllc.oim.matching.vo;

//~--- JDK imports ------------------------------------------------------------

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import oracle.iam.reconciliation.vo.ReconEventData;

/**
 * Class description
 *
 *
 * @version        1.0, 23.Feb 2015
 * @author         Eric A. Fisher, APTEC LLC
 */
public class PartialMatch {
    private Long                    matchID;
    private Long                    reconKey;
    private String                  sourceResource;
    private String                  sourceID;
    private String                  matchedEntityID;
    private Date                    createdOn;
    private Date                    modifiedOn;
    private String                  modifiedBy;
    private String                  matchStatus;
    private HashMap<String, Object> sourceData;
    private List<CandidateMatch>    candidates;

    /**
     * Constructs ...
     *
     */
    public PartialMatch() {
        matchID        = null;
        reconKey       = null;
        sourceResource = null;
        createdOn      = new Date();
        modifiedOn     = new Date();
        modifiedBy     = "SYSTEM";
        matchStatus    = "REVIEW";
        candidates     = new ArrayList<CandidateMatch>();
    }

    /**
     * Method description
     *
     *
     * @param entityID
     * @param score
     * @param candidateData
     *
     * @return
     */
    public CandidateMatch buildCandidate(String entityID, Integer score, HashMap candidateData) {
        CandidateMatch match = new CandidateMatch();

        match.setMatchID(this.matchID);
        match.setCandidateEntityID(entityID);
        match.setMatchScore(score);
        match.setCandidateData(candidateData);

        return match;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public List<CandidateMatch> getCandidates() {
        return candidates;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public Long getMatchID() {
        return matchID;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getMatchStatus() {
        return matchStatus;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getMatchedEntityID() {
        return matchedEntityID;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getModifiedBy() {
        return modifiedBy;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public Date getModifiedOn() {
        return modifiedOn;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public Long getReconKey() {
        return reconKey;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public HashMap<String, Object> getSourceData() {
        return sourceData;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getSourceID() {
        return sourceID;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getSourceResource() {
        return sourceResource;
    }

    /**
     * Method description
     *
     *
     * @param candidates
     */
    public void setCandidates(List<CandidateMatch> candidates) {
        this.candidates = candidates;
    }

    /**
     * Method description
     *
     *
     * @param createdOn
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * Method description
     *
     *
     * @param matchID
     */
    public void setMatchID(Long matchID) {
        this.matchID = matchID;
    }

    /**
     * Method description
     *
     *
     * @param matchStatus
     */
    public void setMatchStatus(String matchStatus) {
        this.matchStatus = matchStatus;
    }

    /**
     * Method description
     *
     *
     * @param matchedEntityID
     */
    public void setMatchedEntityID(String matchedEntityID) {
        this.matchedEntityID = matchedEntityID;
    }

    /**
     * Method description
     *
     *
     * @param modifiedBy
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * Method description
     *
     *
     * @param modifiedOn
     */
    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    /**
     * Method description
     *
     *
     * @param reconKey
     */
    public void setReconKey(Long reconKey) {
        this.reconKey = reconKey;
    }

    /**
     * Method description
     *
     *
     * @param sourceData
     */
    public void setSourceData(HashMap<String, Object> sourceData) {
        this.sourceData = sourceData;
    }

    /**
     * Method description
     *
     *
     * @param sourceID
     */
    public void setSourceID(String sourceID) {
        this.sourceID = sourceID;
    }

    /**
     * Method description
     *
     *
     * @param sourceResource
     */
    public void setSourceResource(String sourceResource) {
        this.sourceResource = sourceResource;
    }
    
    public byte[] getReconEventData() {
    	try {
    		ReconEventData red = (ReconEventData) sourceData.get("ReconEventData");
    		
    		com.aptecllc.oim.matching.vo.ReconEventData redSafe = new com.aptecllc.oim.matching.vo.ReconEventData();
    		
    		for (oracle.iam.reconciliation.vo.ReconTargetAttribute ta :red.getSingleValuedAttrs()) {
    			ReconTargetAttribute rta = new ReconTargetAttribute();
    			rta.setTadName(ta.getTadName());
    			rta.setTadDatatype(ta.getTadDatatype());
    			rta.setNumericVal(ta.getNumericVal());
    			rta.setStringVal(ta.getStringVal());
    			rta.setDateVal(ta.getDateVal());
    			rta.setOimMappedFieldDescription(ta.getOimMappedFieldDescription());
    			redSafe.getSingleValuedAttrs().add(rta);
    		}
    		
    		
			return getSerializedObject(redSafe);
		} catch (Exception e) {
			return null;
		}
    }
    
    private byte[] getSerializedObject(Serializable obj) throws Exception{
    	ByteArrayOutputStream bos = new ByteArrayOutputStream();
    	ObjectOutput out = null;
    	byte[] outBytes = null;
    	
    	
    	  out = new ObjectOutputStream(bos);   
    	  out.writeObject(obj);
    	  outBytes = bos.toByteArray();
    	  if (out != null) {
    	      out.close();
    	    }
    	  bos.close();
    	
    	
    	return outBytes;
    }
}
