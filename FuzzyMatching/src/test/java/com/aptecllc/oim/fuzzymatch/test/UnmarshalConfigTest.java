package com.aptecllc.oim.fuzzymatch.test;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.aptecllc.oim.fuzzymatching.config.*;

import org.junit.Test;

import junit.framework.TestCase;

public class UnmarshalConfigTest extends TestCase {
	
	@Test
	public void testUnmarshalConfig() {
		try {
			 
			File file = new File("src/test/resources/MatchConfiguration.xml");
			JAXBContext jaxbContext = JAXBContext.newInstance(MatchConfiguration.class);
	 
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			MatchConfiguration matchConfig = (MatchConfiguration) jaxbUnmarshaller.unmarshal(file);
			System.out.println("Config: " + matchConfig.getSourceConfiguration().get(0).getMatchSource());
	 
		  } catch (JAXBException e) {
			e.printStackTrace();
		  }
	}

}
