package nyumc.org.iam.oim.plugin.eventhandler;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.identity.vo.Identity;
import oracle.iam.platform.Platform;
import oracle.iam.platform.context.ContextAware;
import oracle.iam.platform.kernel.spi.PostProcessHandler;
import oracle.iam.platform.kernel.vo.AbstractGenericOrchestration;
import oracle.iam.platform.kernel.vo.BulkEventResult;
import oracle.iam.platform.kernel.vo.BulkOrchestration;
import oracle.iam.platform.kernel.vo.EventResult;
import oracle.iam.platform.kernel.vo.Orchestration;
import oracle.iam.platform.utils.logging.SuperLogger;

//public class HREmployeeRehireTasks extends BaseUserPreProcessHandler {
public class HREmployeeRehireTasks implements PostProcessHandler {

	/** Variables for logging */
	private final static Logger logger = SuperLogger.getLogger(HREmployeeRehireTasks.class.getName());


    private String CLASS_NAME = " HREmployeeRehireTasks ";


    public EventResult execute(long l, long l1, Orchestration orchestration) {
        String METHOD_NAME = " Execute";
        logger.info(CLASS_NAME + METHOD_NAME + " Executing Execute Method");
        String orchOperation = orchestration.getOperation();
        String userKey = orchestration.getTarget().getEntityId();
        logger.info(CLASS_NAME + METHOD_NAME + "  User Key  " + userKey);
        HashMap parameterHashMap = orchestration.getParameters();


        try {
            HashMap<String, Serializable> interParameters = orchestration.getInterEventData();
            Object curUserObject = interParameters.get("CURRENT_USER");
            Identity newUsersState  = (Identity) curUserObject;
            executeEvent(parameterHashMap, userKey, orchestration.getTarget().getType(), newUsersState, orchOperation);
        } catch (Exception e) {
        	logger.log(Level.SEVERE, CLASS_NAME + METHOD_NAME + " Exception ", e);
        }
        logger.info(CLASS_NAME + METHOD_NAME + " Exiting Execute Method");


        return new EventResult();
    }


    public BulkEventResult execute(long l, long l1,
                                   BulkOrchestration bulkOrchestration) {

        String METHOD_NAME = ".BulkExecute.";
        logger.info(CLASS_NAME + METHOD_NAME +
                    " Executing BulkExecute Method");
        String orchOperation = bulkOrchestration.getOperation();
        String[] userKeys = bulkOrchestration.getTarget().getAllEntityId();

        HashMap[] parameterHashMap = bulkOrchestration.getBulkParameters();
        HashMap<String, Serializable> interParameters = bulkOrchestration.getInterEventData();
        Object oldUserObject = interParameters.get("CURRENT_USER");
        Identity[] oldUserState  = (Identity[]) oldUserObject;

        // Get new user state
 //       Object newUserObject = interParameters.get("NEW_USER_STATE");
   //     Identity[] newUserState  = (Identity[]) newUserObject;



        for (int i = 0; i < parameterHashMap.length; i++) {
            logger.info(CLASS_NAME + METHOD_NAME + " UserKey " + userKeys[i]);
            try {
                    executeEvent(parameterHashMap[i], userKeys[i],
                                 bulkOrchestration.getTarget().getType(), oldUserState[i], orchOperation);
            } catch (Exception e) {
                logger.log(Level.SEVERE, "Execute Exception ", e);
            }
        }
        logger.info(CLASS_NAME + METHOD_NAME + " Exiting BulkExecute Method");


        return new BulkEventResult();
    }

	
    public void executeEvent(HashMap parameterHashMap, String userKey,
            String targetType, Identity oldUserState, String orchOperation) {
        String METHOD_NAME = ".executeEvent.";
        logger.info(CLASS_NAME + METHOD_NAME + " Entering with parameters " + parameterHashMap);
        // Get new user state
        UserManager usrMgr = Platform.getService(UserManager.class);

        Set<String> retAttrs = new HashSet<String>();
        retAttrs.add("Status");
        String newUserStatus = "Active";
        try {
        	User newUserState = (User) usrMgr.getDetails(userKey, retAttrs, false);
        	newUserStatus=(String) newUserState.getAttribute("Status".toString());
        	System.out.println("New Status from HREmployeeRehireTasks:"+newUserStatus);
        } catch(Exception e) {
        	e.printStackTrace();
        }
        

        // Get old user state
        String oldUserStatus=(String) oldUserState.getAttribute("Status".toString());
        System.out.println("Existing Status from HREmployeeRehireTasks:"+oldUserStatus);

        HashMap<String, Object> attrs = new HashMap<String, Object>();
        try {
        	if (oldUserStatus.equalsIgnoreCase("Disabled") && newUserStatus.equalsIgnoreCase("Active")) {

            	System.out.println("Enable Operation encountered");
            	attrs.put("TerminationDate",(Date)null);
                attrs.put("EmailSent", null);
                attrs.put("TermsAccepted", null);
                Date d =new Date();
                System.out.println("date when user is rehired--->"+d);
                attrs.put("RehireDate", d);
                attrs.put("ActivationStatus", "Active");

                User user = new User(userKey, attrs);
                try {
                    usrMgr.modify(user);
        		} catch (Exception e1) {
        			e1.printStackTrace();
        		}
            }


            }

        catch(Exception e)
        {
        	System.out.println("exception encountered");
        }
    	
    }

	
	
	
	
	
	
	
	
	/*protected EventResult ProcessUserEvent(AbstractGenericOrchestration orch, String entityId, HashMap orchParams, HashMap interEventData,
			Identity currentUserState, Identity newUserState) throws EventFailedException {
		*/

//

	@Override
	public void initialize(HashMap<String, String> arg0) {
		// TODO Auto-generated method stub

	}



    private String getParameterValue(HashMap parameters, String key) {
        String value =
            (parameters.get(key) instanceof ContextAware) ?
            (String) ((ContextAware) parameters.get(key)).getObjectValue() : (String) parameters.get(key);
        return value;
    }



	@Override
	public boolean cancel(long arg0, long arg1,
			AbstractGenericOrchestration arg2) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void compensate(long arg0, long arg1,
			AbstractGenericOrchestration arg2) {
		// TODO Auto-generated method stub

	}
}
