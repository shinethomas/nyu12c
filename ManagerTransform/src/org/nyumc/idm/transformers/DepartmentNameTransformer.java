package org.nyumc.idm.transformers;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import Thor.API.Exceptions.tcAPIException;
import Thor.API.Operations.tcLookupOperationsIntf;
import oracle.iam.identity.exception.UserSearchException;
import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.identity.usermgmt.api.UserManagerConstants.AttributeName;
import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.platform.Platform;
import oracle.iam.platform.entitymgr.vo.SearchCriteria;
import oracle.iam.platform.utils.logging.SuperLogger;

public class DepartmentNameTransformer {
	/** Variables for logging */
	private final static Logger LOGGER = SuperLogger
			.getLogger(UserManagerTransformer.class.getName());

	/*
	 * Description:Abstract method for transforming the attributes param
	 * hmUserDetails<String,Object> HashMap containing parent data details param
	 * hmEntitlementDetails <String,Object> HashMap containing child data
	 * details
	 */

	public Object transform(HashMap hmUserDetails,
			HashMap hmEntitlementDetails, String sField) {
		/*
		 * You must write code to transform the attributes. Parent data
		 * attribute values can be fetched by using
		 * hmUserDetails.get("Field Name").To fetch child data values, loop
		 * through the ArrayList/Vector fetched by
		 * hmEntitlementDetails.get("Child Table") Return the transformed
		 * attribute.
		 */
		LOGGER.logp(Level.INFO, getClass().getName(), "transform",
				"Entered");
		System.out.println("hmUserDetails:"+hmUserDetails.values());
		System.out.println("hmEntitlementDetails:"+hmEntitlementDetails.values());
		System.out.println("hmUserDetails entryset:"+hmUserDetails.entrySet());
		System.out.println("sField:"+sField);

		
		String fieldvalue=(String) (hmUserDetails.get(sField)==null?"":hmUserDetails.get(sField));			
		
		System.out.println("fieldvalue==="+fieldvalue); //have dept id here
		
		 tcLookupOperationsIntf lookupOpsIntf = Platform.getService(tcLookupOperationsIntf.class);
		 String lookupValue = "";
		try {
			lookupValue = lookupOpsIntf.getDecodedValueForEncodedValue(
			         "Lookup.NYU.Department", fieldvalue);
		} catch (tcAPIException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return lookupValue;
		
	}
}
