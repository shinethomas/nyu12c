package org.nyumc.idm.transformers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.iam.platform.Platform;
import oracle.iam.platform.utils.logging.SuperLogger;
import Thor.API.Exceptions.tcAPIException;
import Thor.API.Operations.tcLookupOperationsIntf;

import com.thortech.xl.crypto.*;

public class PSFTEmailrequiredTransformer {
	/** Variables for logging */
	private final static Logger LOGGER = SuperLogger
			.getLogger(UserManagerTransformer.class.getName());

	/*
	 * Description:Abstract method for transforming the attributes param
	 * hmUserDetails<String,Object> HashMap containing parent data details param
	 * hmEntitlementDetails <String,Object> HashMap containing child data
	 * details
	 */

	public Object transform(HashMap hmUserDetails,
			HashMap hmEntitlementDetails, String sField) throws ParseException {
		/*
		 * You must write code to transform the attributes. Parent data
		 * attribute values can be fetched by using
		 * hmUserDetails.get("Field Name").To fetch child data values, loop
		 * through the ArrayList/Vector fetched by
		 * hmEntitlementDetails.get("Child Table") Return the transformed
		 * attribute.
		 */
		LOGGER.logp(Level.INFO, getClass().getName(), "transform",
				"Entered");
		System.out.println("Email Required.java:"+hmUserDetails.values());
		//System.out.println("EmployeeTypeTransformer:"+hmEntitlementDetails.values());
		//System.out.println("EmployeeTypeTransformer entryset:"+hmUserDetails.entrySet());
		//System.out.println(" EmployeeTypeTransformer sField:"+sField);
		//Date date =null;

		
		String fieldvalue=(String) (hmUserDetails.get(sField)==null?"":hmUserDetails.get(sField));	
		String fieldvalueEmailReqd = (String) (hmUserDetails.get("Email Required") == null ? ""
				: hmUserDetails.get("Email Required"));
		System.out
				.println("Email Required: is not null inside Email Required converter in Job record"
						+ fieldvalueEmailReqd);
		if (fieldvalueEmailReqd != null && !fieldvalueEmailReqd.trim().isEmpty()
				&& fieldvalueEmailReqd.equalsIgnoreCase("Y")) {
			return "1";
		}
		
		
		else return "0";
   }
}

	
		
	
	




