package org.nyumc.idm.transformers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.iam.platform.Platform;
import oracle.iam.platform.utils.logging.SuperLogger;
import Thor.API.Exceptions.tcAPIException;
import Thor.API.Operations.tcLookupOperationsIntf;

import com.thortech.xl.crypto.*;

public class PhoneTransformer {
	/** Variables for logging */
	private final static Logger LOGGER = SuperLogger
			.getLogger(UserManagerTransformer.class.getName());

	/*
	 * Description:Abstract method for transforming the attributes param
	 * hmUserDetails<String,Object> HashMap containing parent data details param
	 * hmEntitlementDetails <String,Object> HashMap containing child data
	 * details
	 */

	public Object transform(HashMap hmUserDetails,
			HashMap hmEntitlementDetails, String sField) throws ParseException {
		/*
		 * You must write code to transform the attributes. Parent data
		 * attribute values can be fetched by using
		 * hmUserDetails.get("Field Name").To fetch child data values, loop
		 * through the ArrayList/Vector fetched by
		 * hmEntitlementDetails.get("Child Table") Return the transformed
		 * attribute.
		 */
		LOGGER.logp(Level.INFO, getClass().getName(), "transform",
				"Entered");
		System.out.println("Transform SSN transformation code enetered"+sField);
		System.out.println("hmUserDetails:"+hmUserDetails.values());
		
		System.out.println("hmUserDetails entryset:"+hmUserDetails.entrySet());
		System.out.println("sField:"+sField);
		//System.out.println("hmEntitlementDetails:"+hmEntitlementDetails.values());
		//Date date =null;

		
		String fieldvalue=(String) (hmUserDetails.get(sField)==null?"":hmUserDetails.get(sField));	
		if(fieldvalue!=null && !fieldvalue.isEmpty()){
		try{
		System.out.println("Unformatted phone number is \n"+fieldvalue);
		String firstPart = fieldvalue.substring(0,fieldvalue.indexOf("/"));
		System.out.println("--1st part-->"+firstPart);
		String secondPart = fieldvalue.substring(fieldvalue.indexOf("/")+1,fieldvalue.indexOf("-"));
		System.out.println("--2nd part-->"+secondPart);
		String thirdPart = fieldvalue.substring(fieldvalue.indexOf("-")+1,fieldvalue.length());
		System.out.println("--3nd part-->"+thirdPart);
		StringBuffer st = new StringBuffer();
		st.append("+1");
		st.append(" ");
		st.append(firstPart);
		st.append(" ");
		st.append(secondPart);
		st.append(" ");
		st.append(thirdPart);
		System.out.println("Formatted phone number is\n"+st.toString());
		return st.toString();
		}catch(Exception e){
			return fieldvalue;
		}
		
		}
		else{
			return "";
			}
		
	}

}

