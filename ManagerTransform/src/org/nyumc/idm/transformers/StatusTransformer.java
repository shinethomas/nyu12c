package org.nyumc.idm.transformers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import oracle.iam.platform.Platform;
import oracle.iam.platform.entitymgr.vo.SearchCriteria;
import oracle.iam.platform.utils.logging.SuperLogger;
import Thor.API.tcResultSet;
import Thor.API.tcUtilityFactory;
import Thor.API.Exceptions.tcAPIException;
import Thor.API.Operations.tcLookupOperationsIntf;
import Thor.API.Operations.tcUserOperationsIntf;
import oracle.iam.connectors.psft.common.handler.APIProvider;
import oracle.iam.identity.exception.UserSearchException;
import oracle.iam.identity.usermgmt.api.UserManagerConstants.AttributeName;
import oracle.iam.identity.usermgmt.vo.User;
import com.thortech.xl.crypto.*;

public class StatusTransformer {
  /** Variables for logging */
  private final static Logger LOGGER = SuperLogger
    .getLogger(UserManagerTransformer.class.getName());
  private APIProvider apiProvider = null;
  public tcUtilityFactory apiFactory = null;
  String userEmplid = "";
  String usr_mgr_key = null;
  String usr_status="Active";
  List<User> users = null;

private tcUserOperationsIntf userAPI;

 /*
   * Description:Abstract method for transforming the attributes param
   * hmUserDetails<String,Object> HashMap containing parent data details param
   * hmEntitlementDetails <String,Object> HashMap containing child data
   * details
   */

 public Object transform(HashMap hmUserDetails,
    HashMap hmEntitlementDetails, String sField) throws ParseException {
   /*
    * You must write code to transform the attributes. Parent data
    * attribute values can be fetched by using
    * hmUserDetails.get("Field Name").To fetch child data values, loop
    * through the ArrayList/Vector fetched by
    * hmEntitlementDetails.get("Child Table") Return the transformed
    * attribute.
    */
   LOGGER.logp(Level.INFO, getClass().getName(), "transform",
     "Entered");
   System.out.println("Inside Status Transformer to check for demographics update"+hmUserDetails.values());
   String fieldvalue=(String) (hmUserDetails.get("EMPLID")==null?"":hmUserDetails.get("EMPLID")); 
   this.apiProvider = new APIProvider("xelsysadm", 1,5);
   this.apiProvider.initializeOIMApiFactory();
   userAPI=apiProvider.getUserAPI();
   List<User> users = null;
   if(fieldvalue!=null && !fieldvalue.trim().isEmpty())
   {
   System.out.println("fieldvalue: is not null inside Status converter");
   System.out.println("userEmplid==="+fieldvalue);
   try {

   Set <String>retAttributes = new HashSet<String>(); 
   retAttributes.add(AttributeName.STATUS.getId()); 
   Hashtable mhSearchCriteria = new Hashtable();
   mhSearchCriteria.put("EMPLID", fieldvalue);
   tcResultSet moResultSet = userAPI.findUsers(mhSearchCriteria);
      for (int i=0; i<moResultSet.getRowCount(); i++){
          moResultSet.goToRow(i);
   System.out.println("Status is : "+ String.valueOf(moResultSet.getStringValue("Users.Status")) + " for user id : "+ moResultSet.getStringValue("Users.User ID"));
   usr_status=moResultSet.getStringValue("Users.Status");
   }
 
   System.out.println("usr_status:"+usr_status); 
 
   } catch (Exception e) {
   // TODO Auto-generated catch block
   e.printStackTrace();
   return "Active";
   }
   finally{
	   System.out.println("Before closing the OIMapiFactory");
	   apiProvider.closeOimApiFactory(); 
   }

   }
   System.out.println("Before returning" +usr_status+ "Status");
   return usr_status;
   
  }

}
