package org.nyumc.idm.transformers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.iam.platform.Platform;
import oracle.iam.platform.utils.logging.SuperLogger;
import Thor.API.Exceptions.tcAPIException;
import Thor.API.Operations.tcLookupOperationsIntf;

import com.thortech.xl.crypto.*;

public class WorkForceStatusTransformer {
	/** Variables for logging */
	private final static Logger LOGGER = SuperLogger
			.getLogger(UserManagerTransformer.class.getName());

	/*
	 * Description:Abstract method for transforming the attributes param
	 * hmUserDetails<String,Object> HashMap containing parent data details param
	 * hmEntitlementDetails <String,Object> HashMap containing child data
	 * details
	 */

	public Object transform(HashMap hmUserDetails,
			HashMap hmEntitlementDetails, String sField) throws ParseException {
		/*
		 * You must write code to transform the attributes. Parent data
		 * attribute values can be fetched by using
		 * hmUserDetails.get("Field Name").To fetch child data values, loop
		 * through the ArrayList/Vector fetched by
		 * hmEntitlementDetails.get("Child Table") Return the transformed
		 * attribute.
		 */
		LOGGER.logp(Level.INFO, getClass().getName(), "transform",
				"Entered");
		System.out.println("WorkForce Status Transformer:"+hmUserDetails.values());
		//System.out.println("EmployeeTypeTransformer:"+hmEntitlementDetails.values());
		//System.out.println("EmployeeTypeTransformer entryset:"+hmUserDetails.entrySet());
		//System.out.println(" EmployeeTypeTransformer sField:"+sField);
		//Date date =null;

		
		String fieldvalue=(String) (hmUserDetails.get(sField)==null?"":hmUserDetails.get(sField));	
		String fieldvalueWorkForceStatus = (String) (hmUserDetails.get("Status") == null ? ""
				: hmUserDetails.get("Status"));
		System.out
				.println("WorkForce : is not null inside WorkForceStatus converter in Job record"
						+ fieldvalueWorkForceStatus);
		if (fieldvalueWorkForceStatus != null && !fieldvalueWorkForceStatus.trim().isEmpty()
				&& fieldvalueWorkForceStatus.equalsIgnoreCase("A")) {
			return "Active";
		}
		
		else if(fieldvalueWorkForceStatus != null && !fieldvalueWorkForceStatus.trim().isEmpty()
				&& fieldvalueWorkForceStatus.equalsIgnoreCase("I")) {
			return "Disabled";
		}
		
		else return "Active";
   }
}

	
		
	
	




