package org.nyumc.idm.transformers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.iam.connectors.psft.common.handler.APIProvider;
import oracle.iam.identity.usermgmt.api.UserManagerConstants.AttributeName;
import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.platform.Platform;
import oracle.iam.platform.entitymgr.vo.SearchCriteria;
import oracle.iam.platform.utils.logging.SuperLogger;
import Thor.API.tcResultSet;
import Thor.API.tcUtilityFactory;
import Thor.API.Exceptions.tcAPIException;
import Thor.API.Operations.tcLookupOperationsIntf;
import Thor.API.Operations.tcUserOperationsIntf;

import com.thortech.xl.crypto.*;

public class WorkForceSupervisorTransformer {
	private final static Logger LOGGER = SuperLogger
			.getLogger(UserManagerTransformer.class.getName());
	private APIProvider apiProvider = null;
	public tcUtilityFactory apiFactory = null;
	String userEmplid = "";
	
	
	String usr_emp_type = "Full-Time";
	List<User> users = null;
	

	private tcLookupOperationsIntf lookupAPI;

	/*
	 * Description:Abstract method for transforming the attributes param
	 * hmUserDetails<String,Object> HashMap containing parent data details param
	 * hmEntitlementDetails <String,Object> HashMap containing child data
	 * details
	 */

	public Object transform(HashMap hmUserDetails,
			HashMap hmEntitlementDetails, String sField) throws ParseException {
		/*
		 * You must write code to transform the attributes. Parent data
		 * attribute values can be fetched by using
		 * hmUserDetails.get("Field Name").To fetch child data values, loop
		 * through the ArrayList/Vector fetched by
		 * hmEntitlementDetails.get("Child Table") Return the transformed
		 * attribute.
		 */
		
		// Logic has been updated on 01/31/2017
		//first look into REPORTS_TO field to see if it is not null
		//  if it is not null then - take the value search in OIM for user who have that position number, return that user as manager;break
		//if REPORTS_TO field is blank
		//look for SUPERVISOR_ID field 
		// if it is not null then - take the value search in OIM for user who have that Employee ID  return that user as manager;break
		// else if both REPORTS_TO and SUPERVISOR_ID is blank
		//no manager in OIM
		
		
		
		LOGGER.logp(Level.INFO, getClass().getName(), "transform", "Entered");
		System.out
				.println("Inside Supervisor Transformer to check for MCIT workforce update" + hmUserDetails.values());
		String fieldvalueSupervisorID = (String) (hmUserDetails.get("Supervisor ID") == null ? "": hmUserDetails.get("Supervisor ID"));
		String fieldvalueReportsTo = (String) (hmUserDetails.get("ReportsTo") == null ? "": hmUserDetails.get("ReportsTo"));
		
		System.out.println("ReportsTo id passed from HR is :"+fieldvalueReportsTo);
		System.out.println("Supervisor id passed from HR is :"+fieldvalueSupervisorID);
		
		String usr_mgr_key_reportsto = null;
		String usr_status_reportsto = "";
		
		String usr_mgr_key_supervisorid = null;
		String usr_status_supervisorid = "";
		
		String finalmanagerkey="";
		
		this.apiProvider = new APIProvider("xelsysadm", 1, 5);
		this.apiProvider.initializeOIMApiFactory();
		tcUserOperationsIntf userAPI = apiProvider.getUserAPI();
		   
		List<User> contractorSearchresult =null;
		StringBuffer strEncode=null;
		String strencodeValue = "";
		
		String mgr_user_key =null;
		lookupAPI = apiProvider.getLookupAPI();
		Hashtable mhSearchCriteriaupdated = new Hashtable();
		Hashtable mhSearchCriteriaEMPLID = new Hashtable();
		List<User> users = null;
		tcResultSet moResultSetupdatedEMPLID =null;
		if (fieldvalueSupervisorID != null && !fieldvalueSupervisorID.trim().isEmpty()) {
			
			mhSearchCriteriaEMPLID.put("EMPLID", fieldvalueSupervisorID);
		}
		
		if ((fieldvalueReportsTo != null && !fieldvalueReportsTo.trim().isEmpty()) || (fieldvalueSupervisorID != null && !fieldvalueSupervisorID.trim().isEmpty())) {
			//Added code on Jan 31st 
			//added on 01/24/2018
			
			mhSearchCriteriaupdated.put("PositionNumber", fieldvalueReportsTo);
			String PositionNumber = "PositionNumber";
			String EMPLID = "EMPLID";
			String Status = "Status";
						
			try {
				tcResultSet moResultSetupdated = userAPI.findUsers(mhSearchCriteriaupdated);
				if (fieldvalueSupervisorID != null && !fieldvalueSupervisorID.trim().isEmpty()) {
				 moResultSetupdatedEMPLID = userAPI.findUsers(mhSearchCriteriaEMPLID);
				}
				
				//there are two users tied to same position number
				if(moResultSetupdated!=null && moResultSetupdated.getRowCount()>=1) {
					 System.out.println("Search Result has one value");  
				      for (int i=0; i<moResultSetupdated.getRowCount(); i++){
				    	  moResultSetupdated.goToRow(i);
				   System.out.println("Manager Key from OIM is : "+ String.valueOf(moResultSetupdated.getStringValue("Users.Key")) + " for user with position number : "+ fieldvalueReportsTo);
				   usr_mgr_key_reportsto = moResultSetupdated.getStringValue("Users.Key");
				   usr_status_reportsto = moResultSetupdated.getStringValue("Users.Status");
				   System.out.println("Manager user key in WorkForce Employee Type Transformer:"+usr_mgr_key_reportsto+"and the status of user in OIM is->"+usr_status_reportsto);
				   if(usr_status_reportsto!=null && usr_mgr_key_reportsto!=null ){
						if(usr_status_reportsto.equalsIgnoreCase("Active")){
						
							finalmanagerkey=usr_mgr_key_reportsto;
							return finalmanagerkey;}
					}
				      }
				      }
				if (moResultSetupdated.getRowCount()<=0 || moResultSetupdated.getRowCount()>=2 || usr_status_reportsto.equalsIgnoreCase("Disabled") || usr_mgr_key_reportsto==null)
				{
					System.out.println("there are no specific manager, let us look for Supervisor ID");
					
					if(moResultSetupdatedEMPLID!=null && moResultSetupdatedEMPLID.getRowCount()>=1) {
						 System.out.println("Search Result has one value");  
					      for (int i=0; i<moResultSetupdatedEMPLID.getRowCount(); i++){
					    	  moResultSetupdatedEMPLID.goToRow(i);
					   System.out.println("Manager Key from OIM is : "+ String.valueOf(moResultSetupdatedEMPLID.getStringValue("Users.Key")) + " for user with EMPLID : "+ fieldvalueSupervisorID);
					   usr_mgr_key_supervisorid = moResultSetupdatedEMPLID.getStringValue("Users.Key");
					   usr_status_supervisorid = moResultSetupdatedEMPLID.getStringValue("Users.Status");
					   System.out.println("Manager user key in WorkForce Employee Type Transformer:"+usr_mgr_key_supervisorid+"and the status of user in OIM is->"+usr_status_supervisorid);
					   if(usr_mgr_key_supervisorid!=null && usr_mgr_key_supervisorid!=null ){
							if(usr_status_supervisorid.equalsIgnoreCase("Active")){
							
							finalmanagerkey=usr_mgr_key_supervisorid;
							return finalmanagerkey;}
						}
					      }
					      }
				}
				if(usr_status_reportsto.equalsIgnoreCase("Disabled") || usr_mgr_key_reportsto==null || usr_status_supervisorid.equalsIgnoreCase("Disabled") || usr_mgr_key_supervisorid==null || moResultSetupdatedEMPLID.getRowCount()>=2){
										
					return "";
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
			finally {
				System.out.println("Before closing the OIMapiFactory");
				apiProvider.closeOimApiFactory();
			
		}
	
	
		System.out.println("Before returning usr_manager_key + "+finalmanagerkey);
		

	}
	return finalmanagerkey;
	}
}

