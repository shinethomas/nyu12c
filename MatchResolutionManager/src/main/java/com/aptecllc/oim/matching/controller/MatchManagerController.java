package com.aptecllc.oim.matching.controller;
 
 import com.aptecllc.oim.matching.entity.History;
 import com.aptecllc.oim.matching.entity.Partialmatch;
 import com.aptecllc.oim.matching.entity.Users;
 import com.aptecllc.oim.matching.service.HistoryService;
 import com.aptecllc.oim.matching.service.OIMRestService;
 import com.aptecllc.oim.matching.service.PartialMatchService;
 import com.aptecllc.oim.matching.service.UsersService;
 import java.io.PrintStream;
 import java.math.BigDecimal;
 import java.sql.SQLException;
 import java.util.Arrays;
 import java.util.Date;
 import java.util.List;
 import java.util.Map;
 import javax.servlet.http.HttpServletRequest;
 import javax.servlet.http.HttpSession;
 import org.springframework.beans.factory.annotation.Autowired;
 import org.springframework.security.core.Authentication;
 import org.springframework.security.core.GrantedAuthority;
 import org.springframework.security.core.context.SecurityContext;
 import org.springframework.security.core.context.SecurityContextHolder;
 import org.springframework.stereotype.Controller;
 import org.springframework.ui.Model;
 import org.springframework.web.bind.annotation.ModelAttribute;
 import org.springframework.web.bind.annotation.PathVariable;
 import org.springframework.web.bind.annotation.RequestMapping;
 import org.springframework.web.bind.annotation.RequestParam;
 import org.springframework.web.servlet.ModelAndView;
 
 @Controller
 public class MatchManagerController
 {
 
   @Autowired
   private PartialMatchService partialMatchService;
 
   @Autowired
   private HistoryService historyService;
 
   @Autowired
   private UsersService usersService;
 
   @RequestMapping({"/", "/reviewMatches"})
   public String reviewMatches(Model model)
   {
/*  47 */     Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
 
/*  49 */     String securityrole = "";
/*  50 */     if (authentication != null) {
/*  51 */       System.out.println("reviewMatches:Authentication status:" + authentication.isAuthenticated());
/*  52 */       System.out.println("reviewMatches:Authentication name:" + authentication.getName());
/*  53 */       System.out.println("reviewMatches:Authentication role:" + authentication.getAuthorities());
/*  54 */       for (GrantedAuthority auth : authentication.getAuthorities()) {
/*  55 */         if ("ROLE_ADMIN".equals(auth.getAuthority())) {
/*  56 */           securityrole = "ROLE_ADMIN";
         }
       }
/*  59 */       System.out.println("reviewMatches entered.");
 
/*  62 */       model.addAttribute("pendingMatches", this.partialMatchService.findAll());
 
/*  64 */       model.addAttribute("securityrole", securityrole);
/*  65 */       return "reviewMatches";
     }
 
/*  68 */     return "redirect:/reviewMatches";
   }
 
   @RequestMapping(value={"/submit"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
   public String submit(HttpServletRequest request)
   {
/*  84 */     Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
 
/*  86 */     if (authentication != null) {
/*  87 */       System.out.println("submit:Authentication status:" + authentication.isAuthenticated());
/*  88 */       System.out.println("submit:Authentication name:" + authentication.getName());
/*  89 */       Map parameterMap = request.getParameterMap();
/*  90 */       System.out.println("createNewUser called:" + parameterMap);
/*  91 */       System.out.println("createNewUser called:Action:" + (String)Arrays.asList(request.getParameterValues("Action")).get(0));
 
/*  93 */       System.out.println("createNewUser called:matchID:" + (String)Arrays.asList(request.getParameterValues("matchID")).get(0));
 
/*  95 */       System.out.println("createNewUser called:candidateID:" + (String)Arrays.asList(request.getParameterValues("candidateID")).get(0));
 
/*  98 */       Long matchID = Long.valueOf(((String)Arrays.asList(request.getParameterValues("matchID")).get(0)).toString());
 
/* 101 */       Partialmatch partialmatch = this.partialMatchService.getPartialmatchByID(matchID);
 
/* 103 */       OIMRestService oimRestService = new OIMRestService();
/* 104 */       String action = ((String)Arrays.asList(request.getParameterValues("Action")).get(0)).toString();
 
/* 106 */       String candidateId = (String)Arrays.asList(request.getParameterValues("candidateID")).get(0);
 
/* 108 */       String candidateEntityId = (String)Arrays.asList(request.getParameterValues("candidateEntityId")).get(0);
 
/* 110 */       if (action.equalsIgnoreCase("Create New User")) {
/* 111 */         oimRestService.createNewUser(partialmatch.getReKey().toString());
/* 112 */         System.out.println("create new user called");
/* 113 */       } else if (action.equalsIgnoreCase("Merge User")) {
/* 114 */         oimRestService.linkUser(partialmatch.getReKey().toString(), candidateEntityId);
 
/* 116 */         System.out.println("Link User called");
       }
/* 118 */       partialmatch.setMatchedentityid(candidateEntityId);
/* 119 */       partialmatch.setMatchstatus(action);
/* 120 */       this.partialMatchService.save(partialmatch);
/* 121 */       System.out.println("Partial Match Updated");
/* 122 */       History history = new History();
/* 123 */       history.setComments(action);
/* 124 */       history.setCreatedon(new Date());
/* 125 */       history.setCreatedBy("OIM Engine");
/* 126 */       history.setPartialmatch(partialmatch);
/* 127 */       this.historyService.save(history);
/* 128 */       System.out.println("History Inserted");
/* 129 */       return "redirect:/";
     }
 
/* 132 */     return "redirect:/";
   }
 
   @RequestMapping(value={"reviewMatchResult/{matchID}"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
   public String reviewMatchResults(@PathVariable Long matchID, Model model, HttpSession session)
   {
/* 139 */     Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
 
/* 141 */     if (authentication != null) {
/* 142 */       System.out.println("reviewMatchResults:Authentication status:" + authentication.isAuthenticated());
/* 143 */       System.out.println("reviewMatchResults:Authentication name:" + authentication.getName());
/* 144 */       Partialmatch partialmatch = this.partialMatchService.getPartialmatchByID(matchID);
 
/* 146 */       model.addAttribute("partialmatch", partialmatch);
 
/* 148 */       session.setAttribute("comparisons", this.partialMatchService.getMatchComparisonsByID(matchID));
 
/* 150 */       session.setAttribute("matchid", matchID);
/* 151 */       System.out.println("matchID:" + matchID);
/* 152 */       List histories = this.historyService.findByPartialmatch(partialmatch);
/* 153 */       System.out.println("histories:" + histories);
/* 154 */       model.addAttribute("histories", histories);
 
/* 156 */       return "reviewMatchResult";
     }
 
/* 159 */     return "redirect:/";
   }
 
   @RequestMapping(value={"candidateMatch/{candidateID}"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
   public String getCandidateMatch(@PathVariable Long candidateID, Model model, HttpSession session)
   {
/* 166 */     Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
 
/* 168 */     if (authentication != null) {
/* 169 */       System.out.println("getCandidateMatch:Authentication status:" + authentication.isAuthenticated());
/* 170 */       System.out.println("getCandidateMatch:Authentication name:" + authentication.getName());
/* 171 */       Map comparisons = (Map)session.getAttribute("comparisons");
 
/* 173 */       model.addAttribute("comparison", comparisons.get(candidateID));
/* 174 */       Partialmatch partialmatch = this.partialMatchService.getPartialmatchByID(Long.valueOf(session.getAttribute("matchid").toString()));
 
/* 177 */       model.addAttribute("partialmatch", partialmatch);
/* 178 */       return "template :: candidateMatch";
     }
 
/* 181 */     return "redirect:/";
   }
 
   @RequestMapping({"/users"})
   public String users(Model model)
   {
/* 188 */     Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
 
/* 191 */     if (authentication != null) {
/* 192 */       System.out.println("users:Authentication status:" + authentication.isAuthenticated());
/* 193 */       System.out.println("users:Authentication name:" + authentication.getName());
/* 194 */       System.out.println("users entered.");
 
/* 197 */       model.addAttribute("users", this.usersService.findAllUsers());
/* 198 */       return "users";
     }
 
/* 201 */     return "redirect:/";
   }
 
   @RequestMapping(value={"/user/edit"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
   public ModelAndView editUserPage(@RequestParam(value="id", required=true) Long id)
   {
/* 208 */     ModelAndView modelAndView = new ModelAndView("user-edit");
/* 209 */     System.out.println("edituserpage:id:" + id);
 
/* 212 */     Users userDTO = this.usersService.findUsersById(id);
 
/* 214 */     modelAndView.addObject("userDTO", userDTO);
/* 215 */     return modelAndView;
   }
 
   @RequestMapping(value={"/user/edit"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
   public ModelAndView editingUser(@ModelAttribute Users userDTO, @RequestParam(value="action", required=true) String action)
     throws SQLException
   {
/* 222 */     System.out.println("editingUser:getUserId:" + userDTO.getUserId());
/* 223 */     System.out.println("editingUser:password:" + userDTO.getPassword());
/* 224 */     System.out.println("editingUser:role:" + userDTO.getAuthority());
/* 225 */     System.out.println("editingUser:username:" + userDTO.getUserName());
/* 226 */     System.out.println("editingUser:enabled:" + userDTO.getEnabled());
/* 227 */     ModelAndView modelAndView = new ModelAndView("redirect:/users");
/* 228 */     String message = null;
/* 229 */     String hashPassword = this.usersService.getHashPassword(userDTO.getPassword());
/* 230 */     userDTO.setPassword(hashPassword);
/* 231 */     if (action.equals("save"))
     {
/* 233 */       this.usersService.saveUsers(userDTO);
 
/* 235 */       System.out.println("User Updated:" + userDTO.getUserName());
/* 236 */       message = "User " + userDTO.getUserName() + " was successfully edited";
/* 237 */       modelAndView.addObject("message", message);
     }
 
/* 240 */     if (action.equals("cancel")) {
/* 241 */       System.out.println("User Cancelled:" + userDTO.getUserName());
/* 242 */       message = "User " + userDTO.getUserName() + " edit cancelled";
     }
 
/* 245 */     return modelAndView;
   }
 
   @RequestMapping(value={"/user/delete"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
   public ModelAndView deleteStrategyPage(@RequestParam(value="id", required=true) Long id, @RequestParam(value="phase", required=true) String phase)
   {
/* 254 */     ModelAndView modelAndView = null;
 
/* 256 */     Users userDTO = this.usersService.findUsersById(id);
 
/* 258 */     if (phase.equals("stage")) {
/* 259 */       modelAndView = new ModelAndView("user-delete");
 
/* 261 */       String message = "User " + userDTO.getUserName() + " queued for display.";
/* 262 */       modelAndView.addObject("userDTO", userDTO);
/* 263 */       modelAndView.addObject("message", message);
     }
 
/* 266 */     if (phase.equals("confirm")) {
/* 267 */       modelAndView = new ModelAndView("redirect:/users");
/* 268 */       this.usersService.deleteUsers(userDTO);
/* 269 */       String message = "User " + userDTO.getUserName() + " was successfully deleted";
/* 270 */       modelAndView.addObject("message", message);
     }
 
/* 273 */     if (phase.equals("cancel")) {
/* 274 */       modelAndView = new ModelAndView("redirect:/users");
/* 275 */       String message = "User delete was cancelled.";
/* 276 */       modelAndView.addObject("message", message);
     }
 
/* 279 */     return modelAndView;
   }
 
   @RequestMapping(value={"/user/add"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
   public ModelAndView addUserPage() {
/* 284 */     ModelAndView modelAndView = new ModelAndView("user-add");
/* 285 */     modelAndView.addObject("userDTO", new Users());
/* 286 */     return modelAndView;
   }
 
   @RequestMapping(value={"/user/add"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
   public ModelAndView addingStrategy(@ModelAttribute Users userDTO)
   {
/* 292 */     ModelAndView modelAndView = new ModelAndView("redirect:/users");
/* 293 */     String hashPassword = this.usersService.getHashPassword(userDTO.getPassword());
/* 294 */     userDTO.setPassword(hashPassword);
/* 295 */     this.usersService.saveUsers(userDTO);
/* 296 */     String message = "User " + userDTO.getUserName() + " was successfully added";
/* 297 */     modelAndView.addObject("message", message);
/* 298 */     return modelAndView;
   }
 }
