package com.aptecllc.oim.matching.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the SOURCEFIELDS database table.
 * 
 */
@Entity
@Table(name="SOURCEFIELDS")
@NamedQuery(name="SourceField.findAll", query="SELECT s FROM SourceField s")
public class SourceField implements Serializable, Comparable<SourceField> {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="SOURCEFIELDID")
	@SequenceGenerator(name="SOURCEFIELDS_SOURCEFIELDID_GENERATOR", sequenceName="SEQ_SOURCEID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SOURCEFIELDS_SOURCEFIELDID_GENERATOR")
	private long sourceFieldID;

	@Column(name="CANDIDATEFIELDNAME")
	private String candidateFieldName;

	@Column(name="DISPLAYORD")
	private Long displayOrd;
	
	@Column(name="SOURCEFIELDNAME")
	private String sourceFieldName;

	//bi-directional many-to-one association to UISourceConfig
	@ManyToOne
	@JoinColumn(name="SOURCEID")
	private UISourceConfig uiSourceConfig;

	public SourceField() {
	}

	public long getSourceFieldID() {
		return this.sourceFieldID;
	}

	public void setSourceFieldID(long sourceFieldID) {
		this.sourceFieldID = sourceFieldID;
	}

	

	
	public Long getDisplayOrd() {
		return displayOrd;
	}

	public void setDisplayOrd(Long displayOrd) {
		this.displayOrd = displayOrd;
	}

	

	public String getCandidateFieldName() {
		return candidateFieldName;
	}

	public void setCandidateFieldName(String candidateFieldName) {
		this.candidateFieldName = candidateFieldName;
	}

	public String getSourceFieldName() {
		return sourceFieldName;
	}

	public void setSourceFieldName(String sourceFieldName) {
		this.sourceFieldName = sourceFieldName;
	}

	public UISourceConfig getUiSourceConfig() {
		return this.uiSourceConfig;
	}

	public void setUiSourceConfig(UISourceConfig uiSourceConfig) {
		this.uiSourceConfig = uiSourceConfig;
	}

	
	public int compareTo(SourceField o) {
		if (this.displayOrd > o.getDisplayOrd()) {
			return 1;
		}
		return -1;
	}

}