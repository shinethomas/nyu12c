package com.aptecllc.oim.matching.entity;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.Sort;
import org.hibernate.annotations.SortType;

import java.util.SortedSet;


/**
 * The persistent class for the UISOURCECONFIG database table.
 * 
 */
@Entity
@Table(name="UISOURCECONFIG")
@NamedQuery(name="UISourceConfig.findAll", query="SELECT u FROM UISourceConfig u")
public class UISourceConfig implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="UISOURCECONFIG_SOURCEID_GENERATOR", sequenceName="SEQ_SOURCEID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="UISOURCECONFIG_SOURCEID_GENERATOR")
	private long sourceID;

	@Column(name="SOURCENAME")
	private String sourceName;

	//bi-directional many-to-one association to SourceField
	@OneToMany(mappedBy="uiSourceConfig",fetch = FetchType.EAGER)
	@OrderBy("displayOrd ASC")
	@Sort(type=SortType.NATURAL)

	private SortedSet<SourceField> sourceFields;

	public UISourceConfig() {
	}

	public long getSourceID() {
		return this.sourceID;
	}

	public void setSourceID(long sourceID) {
		this.sourceID = sourceID;
	}

	public String getSourceName() {
		return this.sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public SortedSet<SourceField> getSourceFields() {
		return this.sourceFields;
	}

	public void setSourceFields(SortedSet<SourceField> sourceFields) {
		this.sourceFields = sourceFields;
	}

	public SourceField addSourceField(SourceField sourceField) {
		getSourceFields().add(sourceField);
		sourceField.setUiSourceConfig(this);

		return sourceField;
	}

	public SourceField removeSourceField(SourceField sourceField) {
		getSourceFields().remove(sourceField);
		sourceField.setUiSourceConfig(null);

		return sourceField;
	}

}