package com.aptecllc.oim.matching.repository;

import com.aptecllc.oim.matching.entity.Partialmatch;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

public abstract interface PartialMatchRepository extends CrudRepository<Partialmatch, Long>
{
  public abstract List<Partialmatch> findBymatchstatus(String paramString);
}