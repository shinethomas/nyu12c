package com.aptecllc.oim.matching.repository;

import com.aptecllc.oim.matching.entity.UISourceConfig;
import org.springframework.data.repository.CrudRepository;

public abstract interface UISourceConfigRepository extends CrudRepository<UISourceConfig, Long>
{
}
