package com.aptecllc.oim.matching.service;
 
 import com.aptecllc.oim.matching.entity.History;
 import com.aptecllc.oim.matching.entity.Partialmatch;
 import com.aptecllc.oim.matching.repository.HistoryRepository;
 import java.util.List;
 import org.slf4j.Logger;
 import org.slf4j.LoggerFactory;
 import org.springframework.beans.factory.annotation.Autowired;
 import org.springframework.stereotype.Service;
 
 @Service
 public class HistoryService
 {
   private static final Logger LOGGER = LoggerFactory.getLogger(HistoryService.class);
 
   @Autowired
   private HistoryRepository historyRepository;
 
   public List<History> findByPartialmatch(Partialmatch partialmatch)
   {
    List<History> results = this.historyRepository.findByPartialmatch(partialmatch);
    return results;
   }
 
   public History save(History history) {
     return this.historyRepository.save(history);
   }
 
   public History getPartialmatchByID(Long id)
   {
    return (History)this.historyRepository.findOne(id);
   }
 }