package com.aptecllc.oim.matching.service;

import com.aptecllc.http.client.TokenAuthHeaderRequestInterceptor;
import java.util.Collections;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.web.client.RestTemplate;

public class OIMRestService {
	//private String apiurl = "http://oialcdcdvm001.nyumc.org:14000/OIMRESTServices";
	private String apiurl = "http://iam1-oim.nyumc.org:14000/OIMRESTServices";
	//private String apiurl = "http://iam1-oim-stage.nyumc.org:14000/OIMRESTServices";
	private String apikey = "SelfService";
	private String apisecret = "nyuL4ng0n3";
	private String apiadmin = "xelsysadm";

	public boolean closeEvent(String reconKey) {
		boolean flag = false;
		flag = reconRest(reconKey, "", "closeevent");
		return flag;
	}

	private boolean reconRest(String reconKey, String userKey,
			String reconAction) {
		ClientHttpRequestInterceptor tokenAuthInterceptor = new TokenAuthHeaderRequestInterceptor(
				this.apikey, this.apisecret, this.apiadmin);

		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setInterceptors(Collections
				.singletonList(tokenAuthInterceptor));

		JSONObject request = new JSONObject();

		request.put("Recon-Key", reconKey);
		request.put("Recon-Action", reconAction);
		request.put("User-Key", userKey);
		//String uri = new String("http://oialcdcdvm001.nyumc.org:14000/OIMRESTServices/reconciliation");
		String uri = new String("http://iam1-oim.nyumc.org:14000/OIMRESTServices/reconciliation");
		//String uri = new String("http://iam1-oim-stage.nyumc.org:14000/OIMRESTServices/reconciliation");

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity entity = new HttpEntity(request.toString(), headers);

		System.out.println("calling post before");
		ResponseEntity loginResponse = restTemplate.exchange(uri,
				HttpMethod.POST, entity, String.class, new Object[0]);

		System.out.println("calling post after:"
				+ loginResponse.getStatusCode());

		boolean flag = true;

		System.out.println("Successfully updated");
		return flag;
	}

	public boolean createNewUser(String reKey) {
		boolean flag = false;
		flag = reconRest(reKey, "", "createnew");
		return flag;
	}

	public boolean linkUser(String reKey, String usrKey) {
		boolean flag = false;
		flag = reconRest(reKey, usrKey, "linktoexistinguser");
		return flag;
	}
}