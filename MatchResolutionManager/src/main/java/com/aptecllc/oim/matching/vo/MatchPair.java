package com.aptecllc.oim.matching.vo;

import java.io.Serializable;

public class MatchPair
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  private String fieldName;
  private Object sourceValue;
  private Object candidateValue;
  private boolean valueMatched;

  public boolean isValueMatched()
  {
     boolean matched = false;
    if ((this.sourceValue != null) && (this.candidateValue != null) && 
      (this.sourceValue.equals(this.candidateValue))) {
       matched = true;
    }

    return matched;
  }

  public void setValueMatched(boolean valueMatched) {
    this.valueMatched = valueMatched;
  }

  public Object getCandidateValue()
  {
     return this.candidateValue;
  }

  public String getFieldName()
  {
     return this.fieldName;
  }

  public Object getSourceValue()
  {
    return this.sourceValue;
  }

  public void setCandidateValue(Object candidateValue)
  {
    this.candidateValue = candidateValue;
  }

  public void setFieldName(String fieldName)
  {
     this.fieldName = fieldName;
  }

  public void setSourceValue(Object sourceValue)
  {
    this.sourceValue = sourceValue;
  }
}