package nyumc.org.iam.oim.plugin.NotificationResolver;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.iam.notification.impl.NotificationEventResolver;
import oracle.iam.notification.vo.NotificationAttribute;
import oracle.iam.platform.utils.logging.SuperLogger;

public class NYUEmailNotificationResolver implements NotificationEventResolver {
	
	private final static Logger LOGGER = SuperLogger.getLogger();
	public NYUEmailNotificationResolver() {
		
	}

	public List<NotificationAttribute> getAvailableData(String arg0,
			Map<String, Object> arg1) throws Exception {
		List<NotificationAttribute> list = new ArrayList<NotificationAttribute>();
		return list;
	}

	public HashMap<String, Object> getReplacedData(String eventType, 
			 Map<String, Object> eventParams) throws Exception {
		
		LOGGER.log(Level.INFO, "NYUEmailNotificationResolver: getReplacedData() Entered");
		System.out.println("NYUEmailNotificationResolver: getReplacedData() Entered");
		HashMap<String, Object> resolvedNotificationData = new HashMap<String, Object>();
		
		//Copying all data from eventParams to resolved notification data Map
		for(String key: eventParams.keySet()) {
			resolvedNotificationData.put(key, eventParams.get(key));
			LOGGER.log(Level.INFO, "Variable name: |" + key + "|  Value: |" + eventParams.get(key) + "|");
			System.out.println("Variable name: |" + key + "|  Value: |" + eventParams.get(key) + "|");
		}
		
		LOGGER.log(Level.INFO, "Office365EmailNotificationResolver: getReplacedData() Exited");
		// returning the resolved data with all user information
		return resolvedNotificationData;
	}

}
