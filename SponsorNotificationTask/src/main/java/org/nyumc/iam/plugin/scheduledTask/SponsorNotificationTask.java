package org.nyumc.iam.plugin.scheduledTask;

//~--- non-JDK imports --------------------------------------------------------

import oracle.iam.identity.exception.NoSuchUserException;
import oracle.iam.identity.exception.SearchKeyNotUniqueException;
import oracle.iam.identity.exception.UserLookupException;
import oracle.iam.identity.exception.UserManagerException;
import oracle.iam.identity.exception.UserModifyException;
import oracle.iam.identity.exception.ValidationFailedException;
import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.identity.usermgmt.api.UserManagerConstants;
import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.identity.vo.Identity;
import oracle.iam.notification.api.NotificationService;
import oracle.iam.notification.exception.EventException;
import oracle.iam.notification.exception.MultipleTemplateException;
import oracle.iam.notification.exception.NotificationException;
import oracle.iam.notification.exception.NotificationResolverNotFoundException;
import oracle.iam.notification.exception.TemplateNotFoundException;
import oracle.iam.notification.exception.UnresolvedNotificationDataException;
import oracle.iam.notification.exception.UserDetailsNotFoundException;
import oracle.iam.notification.vo.NotificationEvent;
import oracle.iam.platform.Platform;
import oracle.iam.platform.authz.exception.AccessDeniedException;
import oracle.iam.platform.entitymgr.EntityManager;
import oracle.iam.platform.entitymgr.vo.SearchCriteria;
import oracle.iam.scheduler.vo.StoppableTask;
import oracle.iam.scheduler.vo.TaskSupport;

import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.aptecllc.iam.oim.utils.notificationprovider.APTECEMAILNotificationProvider;
import com.aptecllc.iam.oim.utils.notificationprovider.APTECEMAILPayload;

/**
* Class description
*
*
* @version        1.0.0, 03/10/18
* @author         Shine Thomas, APTEC LLC
*/
public class SponsorNotificationTask extends TaskSupport implements StoppableTask {
    private static final Logger logger = Logger.getLogger(SponsorNotificationTask.class.getName());
    private StringBuffer results = new StringBuffer();
    private String crlf = "\r\n";
    List<String> userList = new ArrayList<String>();
    HashMap<Object,Object> contractorMap15days = new HashMap<Object,Object>();
    HashMap<Object,Object> contractorMap30days = new HashMap<Object,Object>();
    HashMap<Object,Object> contractorMap45days = new HashMap<Object,Object>();
    Map<String,String> userMap = new HashMap<String,String>();
    
    String resultsTo = "";
                String smtpServer = "";
                String resultsFrom = "";
                String scheduledJobName = "";
                
    /**
     * Method description
     *
     *
     * @param attributes
     *
     * @throws Exception
     */
                public void execute(HashMap attributes) throws Exception {
                String methodName="execute";
                logger.logp(Level.FINE, getClass().getName(), methodName,"entering");
                System.out.println("Inside execute method of sponsor notification task");
                scheduledJobName = getName();
        
        logger.logp(Level.INFO, getClass().getName(), methodName,
            "MIA-DUT-001 Scheduled Job: " + scheduledJobName);
            results.append(crlf + "Results from Scheduled Job: " + scheduledJobName);
                
                               
        //List getContractors 
                                System.out.println("Before calling getContractor method");
                                contractorMap15days = getContractors(15);
                                if (contractorMap15days != null && contractorMap15days.size() > 0) {
                                                sponsorNotificationCheck(contractorMap15days, 15);
                                }
                                contractorMap30days = getContractors(30);
                                if (contractorMap30days != null && contractorMap30days.size() > 0) {
                                                sponsorNotificationCheck(contractorMap30days, 30);
                                }
                                contractorMap45days = getContractors(45);
                                if (contractorMap45days != null && contractorMap45days.size() > 0) {
                                                sponsorNotificationCheck(contractorMap45days, 45);
                                }
        
                                
                                logger.logp(Level.INFO,getClass().getName(), methodName,"contractorMap15days size"+contractorMap15days.size());
                                logger.logp(Level.INFO,getClass().getName(), methodName,"contractorMap30days size"+contractorMap30days.size());
                                logger.logp(Level.INFO,getClass().getName(), methodName,"contractorMap45days size"+contractorMap45days.size());
                                
                                System.out.println("After calling getContractor method contractorMap15days size"+contractorMap15days.size());
                                System.out.println("After calling getContractor method contractorMap30days size"+contractorMap30days.size());
                                System.out.println("After calling getContractor method contractorMap45days size"+contractorMap45days.size());
                                
        

        
    }

    /**
     * Method description
     *
     *
     * @return
     */
    @SuppressWarnings("rawtypes")
                public HashMap getAttributes() {
        return null;
    }

    /**
     * Method description
     *
     */
    public void setAttributes() {}

                private HashMap getContractors(int days) {
                                
                String methodName="getContractors";
                logger.logp(Level.FINE, getClass().getName(), methodName,"Entering...");
                System.out.println("Inside getContractors method");
                List<User> sponsoredindividual     = null;
                DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss z");
                                Calendar cal = Calendar.getInstance();
                                cal.add(Calendar.DATE, days);
                                cal.set(Calendar.HOUR_OF_DAY, 0);
                                cal.set(Calendar.MINUTE, 0);
                                cal.set(Calendar.SECOND, 0);
                                cal.set(Calendar.MILLISECOND, 0);

                                Date currentDatePlusdays = cal.getTime();
                                Date todaysdate = new Date();
                                String updatedString= "";
                                System.out.println(dateFormat.format(currentDatePlusdays));
                logger.logp(Level.INFO, getClass().getName(), methodName,"currentDatePlusdays"+currentDatePlusdays);
                UserManager             usrMgr         = Platform.getService(UserManager.class);
       
        HashMap<String, Object> configParams   = new HashMap<String, Object>();
        HashMap<Object, Object> userMap                                  = new HashMap<Object, Object>();      
        int                     startRow       = 1;
        SearchCriteria          crit1          = new SearchCriteria("Role","Contractor",SearchCriteria.Operator.EQUAL);
        SearchCriteria          crit2          = new SearchCriteria("Status", "Active",SearchCriteria.Operator.EQUAL);
        SearchCriteria          crit3          = new SearchCriteria("EMPLID", "S",SearchCriteria.Operator.DOES_NOT_BEGIN_WITH);
        SearchCriteria          crit4          = new SearchCriteria("End Date", todaysdate,SearchCriteria.Operator.GREATER_EQUAL);
        //SearchCriteria          crit5          = new SearchCriteria("End Date", currentDatePlusdays,SearchCriteria.Operator.LESS_EQUAL);
        SearchCriteria          crit5          = new SearchCriteria("End Date", currentDatePlusdays,SearchCriteria.Operator.EQUAL);
        SearchCriteria          crit6                          = new SearchCriteria(crit1, crit2, SearchCriteria.Operator.AND);
        SearchCriteria          crit7                          = new SearchCriteria(crit6, crit3, SearchCriteria.Operator.AND);
        SearchCriteria          crit8                          = new SearchCriteria(crit7, crit4, SearchCriteria.Operator.AND);
        SearchCriteria          Criteria    = new SearchCriteria(crit8, crit5, SearchCriteria.Operator.AND);
        
        System.out.println("Criteria is->"+Criteria.toString());
        Set<String>            retAttrs        = new HashSet<String>();

        retAttrs.add(UserManagerConstants.AttributeName.USER_LOGIN.getId());
        retAttrs.add(UserManagerConstants.AttributeName.ACCOUNT_END_DATE.getId());
        retAttrs.add("SponsorID");
        retAttrs.add("First Name");
        retAttrs.add("Last Name");
        retAttrs.add("VenderName");
       
       
        try {
            
               sponsoredindividual = usrMgr.search(Criteria, retAttrs, configParams);
               System.out.println("Size of total sponsor user population"+sponsoredindividual.size());
            boolean    hasMore = true;

          
                       

               
                    for (Identity identity : sponsoredindividual) {
                        if (identity.getEntityId() != null) {
                            
                            String userLogin = (String) identity.getAttribute(UserManagerConstants.AttributeName.USER_LOGIN.getId());
                            System.out.println("user login from search is:"+userLogin);
                            Date endDate = (Date) identity.getAttribute(UserManagerConstants.AttributeName.ACCOUNT_END_DATE.getId());
                            System.out.println("user end date from search is:"+endDate);
                            String userSponsor = (String) identity.getAttribute("SponsorID");
                            System.out.println("Sponsored Individual's sponsor from search is:"+userSponsor);
                            String userFirstName = (String) identity.getAttribute("First Name");
                            System.out.println("Sponsored Individual's FN from search is:"+userFirstName);
                            String userLastName = (String) identity.getAttribute("Last Name");
                            System.out.println("Sponsored Individual's LN from search is:"+userLastName);
                            String userVendor = (String) identity.getAttribute("VenderName");
                            System.out.println("Sponsored Individual's Vendor from search is:"+userVendor);
                            DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
                            String updatedenddate=df.format(endDate);
                            
                            updatedString=userLogin+"-"+userFirstName+"-"+userLastName+"-"+userVendor+"-"+updatedenddate;         
                            userMap.put(updatedString,userSponsor);
                        }
                        }
                   
            
                   
                           
        } catch (UserManagerException e) {

               logger.logp(Level.SEVERE, getClass().getName(), methodName,
                               "MIA-DUT-012 UserManagerException"+e.getMessage());
               results.append(crlf + "UserManagerException"+e.getMessage());
        }

        return userMap;
    }

                
                private void sponsorNotificationCheck(HashMap<Object, Object> hm, int numofdays) throws NoSuchUserException, UserLookupException, AccessDeniedException{
                    
        //Declare variables needed within the method
        String currentDate = new Date().toString();
        String templateName = "";
                        Set testSet = hm.entrySet();
                        Iterator i = testSet.iterator();
                       System.out.println("number of days being passed in sponsorNotificationCheck is :"+numofdays);
                       UserManager             usrMgr         = Platform.getService(UserManager.class);
                       HashMap<String, Object> configParams   = new HashMap<String, Object>();
                       List<User> sponsoredindividual         = null;
                       String sponsorfirstName=null;
                       String sponsorlastName=null;
                       String sponsoremail=null;
                       String sponsoruserlogin=null;
                       String sponsoruserfirstname=null;
                       String sponsoruserlastname=null;
                       String sponsoruservendor=null;
                       String sponsoruserenddate=null;
                       
                       
                        //Using a while loop to iterate through the HashMap
                       while(i.hasNext()){
                                       Map.Entry me = (Map.Entry)i.next();
                                       
                                        if(me.getValue() != null){

                                                       String hashKey = me.getKey().toString();
                                                       System.out.println("Sponsored User's Information is"+hashKey);
                                                       
                                                       String[] parts = hashKey.split("-");
                                                                           if (parts.length>0){
                                                                                           sponsoruserlogin=parts[0];
                                                                           System.out.println("parts[0]-Login="+parts[0]);
                                                                                           sponsoruserfirstname=parts[1];
                                                                           System.out.println("parts[1]-FN="+parts[1]);
                                                                                           sponsoruserlastname=parts[2];
                                                                           System.out.println("parts[2]-LN="+parts[2]);
                                                                                           sponsoruservendor=parts[3];
                                                                           System.out.println("parts[3]-Vendor="+parts[3]);
                                                                                           sponsoruserenddate=parts[4];
                                                                          System.out.println("parts[4]-enddate="+parts[4]);
                                                                           }
                                                       
                                                       
                                                       String sponsorUserLogin=me.getValue().toString().toUpperCase();
                                                       System.out.println("Sponsored User's Sponsor's login is"+sponsorUserLogin);
                                                      User u = new User(sponsorUserLogin);
                                                       
                                                       SearchCriteria          criteria          = new SearchCriteria(UserManagerConstants.AttributeName.USER_LOGIN.getId(),sponsorUserLogin,SearchCriteria.Operator.EQUAL);
                                                       Set<String>            retAttrs        = new HashSet<String>();

                                                       retAttrs.add("First Name");
                                                       retAttrs.add("Last Name");
                                                       retAttrs.add("Email");
                                                       try {
                                                           
                                                                          sponsoredindividual = usrMgr.search(criteria, retAttrs, configParams);
                                                                          System.out.println("Size of total sponsor user population"+sponsoredindividual.size());
                                                              boolean    hasMore = true;
                                                               
                                                                      for (Identity identity : sponsoredindividual) {
                                                                          if (identity.getEntityId() != null) {
                                                                              
                                                                                          sponsorfirstName =  (String) identity.getAttribute("First Name");
                                                                                          sponsorlastName  =  (String) identity.getAttribute("Last Name");
                                                                                          sponsoremail     =  (String) identity.getAttribute("Email");
                                                                                          System.out.println("sponsors first , last name and email are----->"+sponsorfirstName+"[--]"+sponsorlastName+"[--]"+sponsoremail);
                                                                                          sendSponsorNotificationEmail("Sponsored Individual Expiration Email", sponsoremail, sponsoruserlogin,sponsoruserfirstname, sponsoruserlastname, sponsoruservendor, sponsoruserenddate,sponsorfirstName, sponsorlastName,numofdays);
                                                                          }
                                                                      }    
                                                                
                                                             } catch(Exception e){
                                                                           e.printStackTrace();}     
                                                                   
                                                        }//end if
                                       //}//end if
                       }//end while
}//end method

    /**
     * Method Description
     * This method calls a method to create an email with the provided information then attempts to send the email.
     * 
     * @param templateName
     * @param altEmail
     * @param firstName
     * @param lastName
     * @param userLogin
    * @throws AccessDeniedException 
    * @throws UserLookupException 
    * @throws NoSuchUserException 
     */
private void sendSponsorNotificationEmail(String templateName, String altEmail, String userlogin,String firstName, String lastName, String vendorname,String enddate, String sponsorfirstName, String sponsorlastName, int numofdays) throws NoSuchUserException, UserLookupException, AccessDeniedException {
                               
                               //Declare variables for method
                              Platform.getService(EntityManager.class);
                              NotificationService notifServ = Platform.getService(NotificationService.class);
                              UserManager usrMgr = Platform.getService(UserManager.class);
                              NotificationEvent sponsorNotificationEvent = null;

                               //Use params to call createNotificationEvent method
                              System.out.println("Before sponsorNotificationEvent");
                              //if alternate email is not null
                              //if usermanager key is null
                              
                              //System.out.println("Just before sending email templateName,email, firstname, lastname,userlogin are-"+templateName+"--"+altEmail+"--"+firstName+"--"+lastName+"--"+userLogin);               
                              sponsorNotificationEvent = createNotificationEvent(templateName, altEmail, userlogin,firstName, lastName, vendorname,enddate,sponsorfirstName,sponsorlastName,numofdays);
                              System.out.println("After sponsorNotificationEvent");

                              
                               //Attempt to send e-mail with the given params. 
                               try {
                                              System.out.println("Attempting to send email.");
                                                               notifServ.notify(sponsorNotificationEvent);
                                                               System.out.println("After sending attempt.");
                                                               //After e-mail is sent,     the "email sent" attribute must be toggled to "Y", so another e-mail is not sent                                             
                                                                                                                             
                                                               
                                               //Catching a whole mess of things that can go wrong. With print lines so you know which one it is.        
                                               } catch (UserDetailsNotFoundException e) {
                                                               System.out.println("UserDetailsNotFoundException");
                                                               e.printStackTrace();
                                               } catch (EventException e) {
                                                               e.printStackTrace();
                                                               System.out.println("EventException");
                                               } catch (UnresolvedNotificationDataException e) {
                                                               e.printStackTrace();
                                                               System.out.println("UnresolvedNotificationDataException");
                                               } catch (TemplateNotFoundException e) {
                                                               e.printStackTrace();
                                                               System.out.println("TemplateNotFoundException");
                                               } catch (MultipleTemplateException e) {
                                                               e.printStackTrace();
                                                               System.out.println("MultipleTemplateException");
                                               } catch (NotificationResolverNotFoundException e) {
                                                               e.printStackTrace();
                                                               System.out.println("NotificationResolverNotFoundException");
                                               } catch (NotificationException e) {
                                                               e.printStackTrace();
                                                               System.out.println("NotificationException");
                                               } catch (AccessDeniedException e) {
                                                               e.printStackTrace();
                                                               System.out.println("AccessDeniedException");
                                               } 
                              
                                               }
                                               
               /**
                 * Method Description
                 * This method takes the information passed into it in the parameters and creates a notification event
                 * This method also adds any variables you want to use in the body of the email to the Hashmap map
                 * 
                 * @param templateName
                 * @param altEmail
                 * @param firstName
                 * @param lastName
                 * @param userLogin
                 * @return NotificationEvent
                 * Returns a notification event to send an email to user's alternate email address
               * @throws AccessDeniedException 
                * @throws UserLookupException 
                * @throws NoSuchUserException 
                 */         
               private NotificationEvent createNotificationEvent(String templateName, String altEmail, String userlogin,String firstName, String lastName, String vendorname,String enddate, String sponsorfirstName, String sponsorlastName, int numofdays) throws NoSuchUserException, UserLookupException, AccessDeniedException {
                               
                               
                               //Declare variables and load your dynamic variables into a HashMap to be sent to he Notification Event
                               NotificationEvent event = new NotificationEvent();
                               System.out.println("Template Name in createNotificationEvent: "+templateName);
                               event.setTemplateName(templateName);
                               System.out.println("After setting the template name firstname passed is->"+firstName+"-and the lastname passed is-"+lastName);
       HashMap map = new HashMap();
       
       System.out.println("after populating the hashmap");
       map.put("UserFirstName", firstName.toString());
       map.put("UserLastName", lastName.toString());
       map.put("UserLogin", userlogin.toString());
       if(vendorname!=null){
          map.put("UserVendor", vendorname.toString());
          } else {map.put("UserVendor", "Undefined");}
       
      /* 
       SimpleDateFormat sdfSource = new SimpleDateFormat(
               "MM/dd/yyyy");

                                // parse the string into Date object
                                Date date;
                                String strDate = "";
                                try {
                                                date = sdfSource.parse(enddate.toString());
                                                // create SimpleDateFormat object with desired date format
                                                SimpleDateFormat sdfDestination = new SimpleDateFormat("MM/dd/yyyy");

                                                // parse the date into another format
                                                strDate = sdfDestination.format(date);

                                } catch (ParseException e1) {
                                                // TODO Auto-generated catch block
                                                e1.printStackTrace();
                                }*/
      
       
       
       map.put("UserEndDate", enddate.toString());
       map.put("SponsorFirstName", sponsorfirstName.toString());
       map.put("SponsorLastName", sponsorlastName.toString());
       map.put("numofdays", numofdays);
       
       List<String> emailToList = new ArrayList<String>();
       event.setParams(map);
       emailToList.add(altEmail);
       
       
       
       //Finish off setting up the e-mail notification event, then return the event to be sent.
       APTECEMAILPayload emailPayload = new APTECEMAILPayload(emailToList, "oimadmin@nyumc.org", true, "UTF-8");
       
       //Creating a new list to house the CC email addresses)
       List<String> ccEmails = new ArrayList<String>();
       ccEmails.add("NYUSecAdminTeam@nyumc.org");
       
       //Putting the from email address and CC email list into the payload to be sent
       emailPayload.setSenderEmailID("oimadmin@nyumc.org");
       emailPayload.setCcMailIds(ccEmails);
       //emailPayload.setBccEmailIds(<OPTIONAL-LISTOFBCCADDRESSES>);
       APTECEMAILNotificationProvider notProvider = new APTECEMAILNotificationProvider(event, emailPayload, "http://iam1-soa.nyumc.org:8001/ucs/messaging/webservice");
       try {
                                               if (notProvider.sendMessage())
                                                  logger.logp(Level.FINE, getClass().getName(), "sendEmailNotificationToUser", "Email Sent Successfully to User with Email Address '" +   "'.");
                                               } catch (Exception e) {
                                                               e.printStackTrace();
                                               }
       
       return event;
               }

}