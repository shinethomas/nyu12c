package com.don;

import java.security.SignatureException;
import java.util.Date;
import java.util.UUID;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

public class APITokenType {
	
	private static final String HMAC_ALGORITHM = "HmacSHA256";
	
	private String _userName;
	private String _apiKey;
	private String _nonce;
	private String _timestamp;
	private String _signature;
	private String _fullKey;
	
	//Generate New Token w/Nonce and timestamp
	public APITokenType(String pApiKey, String pUserName) {
		Date now = new Date();
		_apiKey = pApiKey;
		_userName = pUserName;
		_nonce = generateNonce();
		_timestamp = String.valueOf(now.getTime());
		
		
		
	}
	
	public String generateAPIToken(String secretKey) {
		String message = getMessage();
		String signature = getSignature(secretKey);
		
		return message + ":" + signature;
			
	}
	
	public APITokenType (String pFullKey) throws Exception {
		
		if (Base64.isArrayByteBase64(pFullKey.getBytes())) {
			_fullKey = new String(Base64.decodeBase64(pFullKey.getBytes()));
		} else {
		_fullKey= pFullKey;}
		parseFullKey();
	}
	
	//Key Format is ApiKey:userName:Nonce:timeStamp:Signature
	private void parseFullKey() throws Exception{
		String [] keyParts = _fullKey.split(":");
		
		if (keyParts.length != 5) {
			throw new Exception("Error");
		}
		
		_apiKey = keyParts[0];
		_userName = keyParts[1];
		_nonce = keyParts[2];
		_timestamp = keyParts[3];
		_signature = keyParts[4];
	}
	
	
	public String getUserName() {
		return _userName;
	}

	public void setUserName(String _userName) {
		this._userName = _userName;
	}

	public String getApiKey() {
		return _apiKey;
	}

	public void setApiKey(String _apiKey) {
		this._apiKey = _apiKey;
	}
	
	public boolean validateKey(String secretKey, boolean skipCheckTime) {
		boolean valid = false;
	
		
		if (_signature.equals(getSignature(secretKey))) {
			valid = true;
		}
		
		if(!skipCheckTime) {
			long curTime = new Date().getTime();
			long tokTime = Long.parseLong(_timestamp);
			
			if (curTime < tokTime - 15000 || curTime > tokTime +15000) {
				valid = false;
			} 
		}
		
		
		return valid;
	}
	
	public String getMessage() {
		return  _apiKey + ":" + _userName + ":" + _nonce + ":" + _timestamp;
	}
	
	public String getSignature(String secretKey) {
		String signature = "";
		String message = getMessage();
		try {
			signature = calculateHMAC(message, secretKey);
		} catch (SignatureException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
		
		return signature;
	}

	private static String calculateHMAC(String data, String key)
			throws java.security.SignatureException
			{
			String result;
			try {

			// get an hmac_sha1 key from the raw key bytes
			SecretKeySpec signingKey = new SecretKeySpec(key.getBytes(), HMAC_ALGORITHM);

			// get an hmac_sha1 Mac instance and initialize with the signing key
			Mac mac = Mac.getInstance(HMAC_ALGORITHM);
			mac.init(signingKey);

			// compute the hmac on input data bytes
			byte[] rawHmac = mac.doFinal(data.getBytes());

			// base64-encode the hmac
			byte[] resultBytes = Base64.encodeBase64(rawHmac);
			
			result = new String(resultBytes);
			

			} catch (Exception e) {
			throw new SignatureException("Failed to generate HMAC : " + e.getMessage());
			}
			return result;
			}
	
	private String generateNonce() {
		return UUID.randomUUID().toString();
	}
	
	
	public static void main(String [] args){
		final String API_KEY="SelfService";
		final String SECRET="nyuL4ng0n3";
		
		APITokenType token = new APITokenType(API_KEY,"xelsysadm");
		String generatedToken = token.generateAPIToken(SECRET);
		System.out.println(generatedToken);
	}
	
}