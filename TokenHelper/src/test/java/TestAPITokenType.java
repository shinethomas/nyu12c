
import static org.junit.Assert.*;


import org.junit.Test;

import com.don.APITokenType;

public class TestAPITokenType {
	private static final String API_KEY="SelfService";
	private static final String SECRET="nyuL4ng0n3";

	

	@Test
	public void testGenerateAPIToken() {
		
		APITokenType token = new APITokenType(API_KEY,"xelsysadm");
		
		
		String generatedToken = token.generateAPIToken(SECRET);
		System.out.println(generatedToken);
		
		APITokenType checkToken =null;
		try {
			checkToken = new APITokenType(generatedToken);
			System.out.println(checkToken.generateAPIToken(SECRET));
			assertEquals("Equals Test",generatedToken, checkToken.generateAPIToken(SECRET));
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		assertTrue( "Matched Signature",checkToken.validateKey(SECRET,false));
		
		assertFalse("UnMatched Signature",checkToken.validateKey("noMatch",false));
		
		
	}



}

