package nyumc.org.iam.oim.plugin.eventhandler;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Logger;

import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.platform.Platform;
import oracle.iam.platform.context.ContextAware;
import oracle.iam.platform.kernel.spi.PostProcessHandler;
import oracle.iam.platform.kernel.vo.AbstractGenericOrchestration;
import oracle.iam.platform.kernel.vo.BulkEventResult;
import oracle.iam.platform.kernel.vo.BulkOrchestration;
import oracle.iam.platform.kernel.vo.EventResult;
import oracle.iam.platform.kernel.vo.Orchestration;
import oracle.iam.platform.utils.logging.SuperLogger;

//public class HREmployeeRehireTasks extends BaseUserPreProcessHandler {
public class UserDisableTasks implements PostProcessHandler {

	/** Variables for logging */
	private final static Logger LOGGER = SuperLogger.getLogger(UserDisableTasks.class.getName());

	
	/*protected EventResult ProcessUserEvent(AbstractGenericOrchestration orch, String entityId, HashMap orchParams, HashMap interEventData,
			Identity currentUserState, Identity newUserState) throws EventFailedException {
		*/
	
//

	@Override
	public void initialize(HashMap<String, String> arg0) {
		// TODO Auto-generated method stub
		
	}



	 private String getParamaterValue(HashMap<String, Serializable> parameters, String key) 
	    {
	        if (parameters.containsKey(key)) 
	        {
	            String value = (parameters.get(key) instanceof ContextAware) ? (String)((ContextAware)parameters.get(key)).getObjectValue() : (String)parameters.get(key);
	            return value;
	        } 
	         
	        else
	        {
	            return null;
	        }
	    }

	

	@Override
	public boolean cancel(long arg0, long arg1,
			AbstractGenericOrchestration arg2) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void compensate(long arg0, long arg1,
			AbstractGenericOrchestration arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public EventResult execute(long arg0, long arg1, Orchestration orchestration) {
		String orchOperation = orchestration.getOperation();
        String userKey = orchestration.getTarget().getEntityId();
        EventResult rval = new EventResult();
        HashMap<String, Object> attrs = new HashMap<String, Object>();
        UserManager usrMgr = Platform.getService(UserManager.class);

        try {
            if (orchOperation.equalsIgnoreCase("DISABLE")) {

            	System.out.println("Disable Operation encountered");
                attrs.put("ActivationStatus", "Disabled");
            	attrs.put("RehireDate",(Date)null);

                User user = new User(userKey, attrs);
                try {
                    usrMgr.modify(user);
        		} catch (Exception e1) {
        			e1.printStackTrace();
        		}
            }
        }

        catch(Exception e)
        {
        	System.out.println("exception encountered");
        }


		return rval;
	}

	@Override
	public BulkEventResult execute(long arg0, long arg1, BulkOrchestration arg2) {
		// TODO Auto-generated method stub
		return null;
	}
	
	//
	
}
