package org.nyumc.iam.policy;

//~--- non-JDK imports --------------------------------------------------------

import Thor.API.Exceptions.tcAPIException;
import Thor.API.Exceptions.tcColumnNotFoundException;
import Thor.API.Exceptions.tcInvalidLookupException;
import Thor.API.Operations.tcLookupOperationsIntf;
import Thor.API.tcResultSet;
import oracle.iam.identity.exception.UserNameGenerationException;
import oracle.iam.identity.usermgmt.api.UserNameGenerationPolicy;
import oracle.iam.platform.Platform;

import org.apache.commons.dbutils.DbUtils;

import com.thortech.xl.crypto.*;
import com.thortech.xl.client.dataobj.tcDataBaseClient;
import com.thortech.xl.dataaccess.tcDataSet;
import com.thortech.xl.dataaccess.tcDataSetException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;


/**
* Class description
*
*
* @version        1.0.3
* @author         Eric Fisher, Shine Thomas, APTEC LLC    
*/
public class UserIDPolicy implements UserNameGenerationPolicy {
  private static final Logger logger = Logger.getLogger(UserIDPolicy.class.getName());

  /**
   * Method description
   *
   *
   * @param arg0
   *
   * @return
   */
  public String getDescription(Locale arg0) {
      return "User ID Generation Policy";
  }

  // Take first initial for each name and append an incrementing number until
  // unique. Use x for no middle name initial.

  /**
   * Method description
   *
   *
   * @param reqData
   *
   * @return
   *
   * @throws UserNameGenerationException
   */
  public String getUserName(Map<String, Object> reqData) throws UserNameGenerationException {
  	Pattern allChars = Pattern.compile("[a-zA-Z]");
  	String userName    = "";
  	String netID       = "";
  	String decryptSSN  = "";
  	      
      String firstName  = (reqData.get("First Name") == null)
                          ? null
                          : reqData.get("First Name").toString();
      String lastName   = (reqData.get("Last Name") == null)
                          ? null
                          : reqData.get("Last Name").toString();
      String DOB   = (reqData.get("DOB") == null)
                          ? null
                          : reqData.get("DOB").toString();
      String SSN   = (reqData.get("SSN") == null)
              ? null
              : reqData.get("SSN").toString();
      if(SSN !=null && !SSN.isEmpty()){
      System.out.println("SSN and DOB is : " + SSN + "--"+DOB); 
      try {
        //System.out.println("before decryption");  
    	  
		decryptSSN = tcCryptoUtil.decrypt(SSN,"DBSecretKey" );
    	 
		//System.out.println("Enecrypted attribute is "+tcCryptoUtil.encrypt(decryptSSN, "alias"));
		//System.out.println("decrypted SSN"+decryptSSN);
		//System.out.println("formatted date"+getDOBinWSQFormat(DOB));
			} catch (tcCryptoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
      
      
        //System.out.println("DOB formatted in WSQ format is : "+getDOBinWSQFormat(DOB));
      DBUtil dbutil = new DBUtil();
      netID = dbutil.getDatafromWSQSP(decryptSSN,getDOBinWSQFormat(DOB));
      //fooString1.equals(fooString2);
      if (netID!=null && !netID.isEmpty() && !netID.equals("nonetid"))
      {
    	  
    	  System.out.println("Value returned from Washington Square->"+netID);
    	  userName = netID;
    	  return userName.toUpperCase();
      }
      }
      logger.log(Level.FINE, "First and Last name is : " + firstName + "--"+lastName);
      
      System.out.println("First and Last name is : " + firstName + "--"+lastName);
     
      if ((lastName == null) || (lastName.length() == 0)) {
          throw new UserNameGenerationException("Last Name is a required entry.", "");
      }

      // Create root
      StringBuffer initials = new StringBuffer();

      //initials.append(firstName.substring(0, 1)).append(middleName.substring(0, 1)).append(lastName.substring(0, 1));
      lastName = lastName.replaceAll("[^\\p{Alpha}\\p{Digit}]+","");
      System.out.println("Lastname stripped out of special character is"+lastName);
      if(lastName.length()>=5){
    	  initials.append(lastName.substring(0,5)).append(firstName.substring(0,1));
      }
      else if (lastName.length()<5){
          initials.append(lastName).append(firstName.substring(0,1));
      }
      
      logger.log(Level.FINE, "NYU UserID is : " + initials.toString());

     /* if (!isUserIDRootAcceptable(initials.toString())) {

          // Not acceptable replace middle initial with X
          initials = new StringBuffer();
          initials.append(firstName.substring(0, 1)).append("X").append(lastName.substring(0, 1));
          logger.log(Level.FINE, "CaneID Root is now : " + initials.toString());
      }*/

      int i = 0;

      do {
          //i++;
    	  //String.format("%02d", i++);
          //userName = initials.toString() + i;
    	  userName = initials.toString() + String.format("%02d", ++i);
          logger.log(Level.FINE, "Trying UserID: " + userName);
      } while (!isUserNameAvailable(userName));

      logger.log(Level.FINE, "Found Unique UserID: " + userName);
      System.out.println("Found Unique UserID: " + userName);
  	 
      return userName.toUpperCase();
  	
  }

  /**
   * Method description
   *
   *
   * @param arg0
   * @param arg1
   *
   * @return
   */
  public boolean isGivenUserNameValid(String arg0, Map<String, Object> arg1) {

      // TODO Auto-generated method stub
      return true;
  }

  public String  getDOBinWSQFormat(String strDOB) {

	  SimpleDateFormat format = new SimpleDateFormat("EEE MMM "+ "dd HH:mm:ss z yyyy");
	  Date tempDate = new Date();
	//Try to parse the parameter date.
	try {
	tempDate = format.parse(strDOB);
	}
	//Catch an error during parsing.
	catch (ParseException e){
	e.printStackTrace();
	return null;
	}
	//Setup a new DateFormat with the format we want and return the date we wanted. 
	String formattedDate = new SimpleDateFormat("yyyyMMdd").format(tempDate);
	 return formattedDate.trim();
	}

      
     
  

  /**
   * Method description
   *
   *
   * @param attrMap
   *
   * @return
   */
  protected Map<String, Object> convertToObjectMap(Map<String, String> attrMap) {
      if (attrMap == null) {
          return null;
      }

      Map attrObjMap = new HashMap(attrMap.size());

      for (Map.Entry entry : attrMap.entrySet()) {
          attrObjMap.put(entry.getKey(), entry.getValue());
      }

      return attrObjMap;
  }

  /**
   * Method description
   *
   *
   * @param arg0
   *
   * @return
   *
   * @throws UserNameGenerationException
   */
  public String getUserNameFromPolicy(Map<String, String> arg0) throws UserNameGenerationException {
      return getUserName(convertToObjectMap(arg0));
  }

  /**
   * Method description
   *
   *
   * @param arg0
   * @param arg1
   *
   * @return
   */
  public boolean isUserNameValid(String arg0, Map<String, String> arg1) {
      return isGivenUserNameValid(arg0, convertToObjectMap(arg1));
  }

  // Probably will need to check an audit table somewhere too.
  private boolean isUserNameAvailable(String userName) {
      boolean           available = true;
      Connection        dbConn    = null;
      PreparedStatement ps        = null;
      ResultSet         rs        = null;

      try {
          dbConn = Platform.getOperationalDS().getConnection();
          ps     = dbConn.prepareStatement("SELECT USR_LOGIN from USR where UPPER(USR_LOGIN) = UPPER(?)");
          ps.setString(1, userName);
          rs = ps.executeQuery();

          if (rs.next()) {
              available = false;
          }
      } catch (SQLException e) {
          logger.log(Level.SEVERE, e.getMessage());
      } finally {
      	 DbUtils.closeQuietly(dbConn, ps, rs);
      }
      
      /*if (available) {
      
	        try {
	            dbConn = Platform.getOperationalDS().getConnection();
	            ps     = dbConn.prepareStatement("SELECT UPPER(IDVALUE) from MIAMI_IDAUDIT where UPPER(IDVALUE) = UPPER(?)");
	            ps.setString(1, userName);
	            rs = ps.executeQuery();
	
	            if (rs.next()) {
	                available = false;
	            }
	        } catch (SQLException e) {
	            logger.log(Level.SEVERE, e.getMessage());
	        } finally {
	            DbUtils.closeQuietly(dbConn, ps, rs);
	        }  
      }*/
      
     /* if (available) {
      	try{
      	 dbConn = Platform.getOperationalDS().getConnection();
           ps     = dbConn.prepareStatement("INSERT INTO MIAMI_IDAUDIT(IDVALUE) values (?)");
           ps.setString(1, userName);
           ps.executeUpdate();

          
       } catch (SQLException e) {
      	 //If it errors then it is not available which should seldom happen since we just checked.
           logger.log(Level.SEVERE, e.getMessage());
           available = false;
       } finally {
           DbUtils.closeQuietly(dbConn, ps, rs);
       }  
      }*/
      	

      return available;
  }


  private boolean isUserIDRootAcceptable(String caneRoot) {
      boolean                acceptable = true;
      tcLookupOperationsIntf lookup     = Platform.getService(tcLookupOperationsIntf.class);

      try {
          tcResultSet result = lookup.getLookupValues("Lookup.NYU.RestrictedUserIDs");

          if (result.getRowCount() > 0) {
              int rowCount = result.getRowCount();

              for (int i = 0; i < rowCount; i++) {
                  result.goToRow(i);

                  String restrictedValue = result.getStringValue("lkv_encoded");

                  if (caneRoot.equalsIgnoreCase(restrictedValue)) {
                      acceptable = false;

                      break;
                  }
              }
          }
      } catch (tcAPIException e) {
          logger.severe(e.getMessage());
      } catch (tcInvalidLookupException e) {
          logger.severe(e.getMessage());
      } catch (tcColumnNotFoundException e) {
          logger.severe(e.getMessage());
      }

      return acceptable;
  }
}
