package com.nyu.custom;


import Thor.API.Exceptions.tcAPIException;
import Thor.API.Operations.tcUserOperationsIntf;
import Thor.API.tcResultSet;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.platform.Platform;
import oracle.iam.platform.kernel.spi.PostProcessHandler;
import oracle.iam.platform.kernel.vo.AbstractGenericOrchestration;
import oracle.iam.platform.kernel.vo.BulkEventResult;
import oracle.iam.platform.kernel.vo.BulkOrchestration;
import oracle.iam.platform.kernel.vo.EventResult;
import oracle.iam.platform.kernel.vo.Orchestration;
import oracle.iam.platform.utils.logging.SuperLogger;

//Initial 0817
public class NYUAdmDeptUpdate implements PostProcessHandler {
    
     private static final Logger LOGGER = SuperLogger.getLogger(NYUAdmDeptUpdate.class.getName());
     
  
        public EventResult execute(long processId, long eventId,
  
                    Orchestration orchestration) {
  
              LOGGER.entering("NYUAdmDeptUpdate","execute");
  
              LOGGER.info("orchestration.getOperation() -> "+ orchestration.getOperation());
  
             
              HashMap<String, Serializable> orchParams = orchestration.getParameters();
              HashMap<String, Serializable> interParameters = orchestration.getInterEventData(); 
              User currentUserState = (User) interParameters.get("CURRENT_USER");
              String userLogin = null;
              String empType = null;
              userLogin = currentUserState.getLogin();;
              empType = currentUserState.getEmployeeType();
              LOGGER.info("Emp type of user "+userLogin+" is "+empType);
              
              if(orchestration.getOperation().equalsIgnoreCase("MODIFY") && (!empType.equalsIgnoreCase("Contractor")))
              {
  
                
  
                Set<String> paramSet = orchParams.keySet();
  
              String AdminDept = null;
              

  
              for (String key : paramSet) {
  
                    Serializable serializable = orchParams.get(key);
                       
                    if (key.equalsIgnoreCase("AdminDept") && serializable!=null) {
  
                          AdminDept = serializable.toString();
  
                          LOGGER.info("AdminDept of usr "+userLogin+"changed to->" + AdminDept);
  
                          changeSIAdminDept(userLogin,AdminDept );
  
                    }
  
                   
  
              }
  
             LOGGER.exiting("NYUAdmDeptUpdate","execute");
             
              }
  
              return new EventResult();
  
        }
  
        private String changeSIAdminDept(String userLogin, String AdminDept) {
  
              String response = null;
              String SILogin=null;
              
             LOGGER.entering("NYUAdmDeptUpdate", "changeSIAdminDept");
 
             Map<String, String> phAttributeList = new HashMap<String, String>();
             //Get all COntractors whose sponsorid is the usrlogin
             phAttributeList.put("SponsorID", userLogin);
             phAttributeList.put("usr_emp_type", "Contractor");
 
             tcResultSet resultSet = null;
 
             try {
 
                   tcUserOperationsIntf tcUserOp = ( Platform.getService(tcUserOperationsIntf.class));
 
                   resultSet = tcUserOp.findUsers(phAttributeList);
 
                   int count = 0 ;
 
                   count = resultSet.getRowCount();
 
                   LOGGER.info("sponsor count for "+userLogin+"->" + count);
 
                   Map<String, String> SIAttrs = new HashMap<String, String>();
                   tcResultSet SIresultSet = null;
 
                   for (int i = 0; i < count; i++) {
                         LOGGER.info(" Updateing SI # "+ i);
                         resultSet.goToRow(i);
                         SILogin = resultSet.getStringValue("USR_LOGIN");
                         LOGGER.info(i+" SI Login is "+ SILogin);
                         //Get all COntractors whose sponsorid is the usrlogin
                         SIAttrs.put("USR_LOGIN", SILogin);
                         SIresultSet =tcUserOp.findUsers(SIAttrs);
 
                         Map<String, String> updateUser = new HashMap<String, String>();
 
                         updateUser.put("AdminDept", AdminDept);
 
                         tcUserOp.updateUser(SIresultSet, updateUser);
                         LOGGER.info(i+" Updated AdminDept for  "+ SILogin+" to --> "+AdminDept);
 
                   }
                   
                   
                   tcUserOp.close();
 
             } catch (tcAPIException e) {
 
                   LOGGER.info(e.getMessage());
 
             } catch (Exception e) {
 
                 LOGGER.info(e.getMessage());
 
             }
             
            LOGGER.exiting("NYUAdmDeptUpdate","changeSIAdminDept");
 
             return response;
 
       }
 
       public BulkEventResult execute(long l, long l1,
 
                   BulkOrchestration bulkOrchestration) {
 
             return new BulkEventResult();
 
       }
 
       public void compensate(long l, long l1,
 
                   AbstractGenericOrchestration abstractGenericOrchestration) {
 
       }
 
       public boolean cancel(long l, long l1,
 
                   AbstractGenericOrchestration abstractGenericOrchestration) {
 
             return false;
 
       }
 
       public void initialize(HashMap<String, String> hashMap) {
 
       }
 
       /* private String getParameterValue(HashMap<String, Serializable> parameters,
 
                   String key) {
 
             String value = (parameters.get(key) instanceof ContextAware) ? (String) ((ContextAware) parameters
 
                         .get(key)).getObjectValue()
 
                         : (String) parameters.get(key);
 
             return value;
 
       } */
 

 
 }
 
