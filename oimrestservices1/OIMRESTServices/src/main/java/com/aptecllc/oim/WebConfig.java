package com.aptecllc.oim;

import java.util.Properties;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.accept.ContentNegotiationManager;
import org.springframework.web.accept.ContentNegotiationManagerFactoryBean;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
@Configuration
@EnableWebMvc

public class WebConfig extends WebMvcConfigurationSupport{
 @Bean
 public RequestMappingHandlerMapping requestMappingHandlerMapping() {
	 
  RequestMappingHandlerMapping handlerMapping = super.requestMappingHandlerMapping();
  handlerMapping.setUseSuffixPatternMatch(false);
  handlerMapping.setContentNegotiationManager(mvcContentNegotiationManager());
  return handlerMapping;
 } 
 
 @Bean
 public ContentNegotiationManager	mvcContentNegotiationManager()  {
	// ContentNegotiationManager contentNegotiationManagerFactoryBean= super.mvcContentNegotiationManager();
	 
	    ContentNegotiationManagerFactoryBean contentNegotiationManagerFactoryBean = new ContentNegotiationManagerFactoryBean();
	    contentNegotiationManagerFactoryBean.setFavorParameter(true);
	    contentNegotiationManagerFactoryBean.setIgnoreAcceptHeader(true);
	    contentNegotiationManagerFactoryBean.setDefaultContentType(MediaType.APPLICATION_JSON_UTF8);
	    Properties mediaTypesProperties = new Properties();
	    mediaTypesProperties.setProperty("json", MediaType.APPLICATION_JSON_UTF8_VALUE);
	    mediaTypesProperties.setProperty("xml", MediaType.APPLICATION_XML_VALUE);

	    contentNegotiationManagerFactoryBean.setMediaTypes(mediaTypesProperties);
	    contentNegotiationManagerFactoryBean.afterPropertiesSet();
		return contentNegotiationManagerFactoryBean.getObject();
	 
	// return content;
 }

}
