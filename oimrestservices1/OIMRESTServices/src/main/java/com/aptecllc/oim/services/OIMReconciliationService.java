package com.aptecllc.oim.services;

import oracle.iam.reconciliation.api.EventMgmtService;
import oracle.iam.reconciliation.api.ReconOperationsService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Thor.API.Exceptions.tcAPIException;

import com.aptecllc.oim.controller.OIMRESTController;
import com.aptecllc.oim.property.ServiceProperty;
import com.aptecllc.oim.util.v2.OimClient;
import com.aptecllc.oim.vo.Reconciliation;

@Service
public class OIMReconciliationService {
	private static final Logger logger = LoggerFactory
			.getLogger(OIMRESTController.class);
	@Autowired
	private ServiceProperty serviceProperties;

	public boolean createNew(Reconciliation reconObject) {
		System.out.println("Create New User for Recon Key:"
				+ reconObject.getReconKey());
		System.out.println("Create New User for Recon Action:"
				+ reconObject.getReconAction());
		System.out.println("serviceProperties:" + serviceProperties);
		System.out.println("serviceProperties:"
				+ serviceProperties.getOimProviderUrl());
		System.out.println("serviceProperties:"
				+ serviceProperties.getOimAdmUsr());
		System.out.println("serviceProperties:"
				+ serviceProperties.getOimAdmPwd());
		OimClient oimClient = new OimClient(serviceProperties);
		EventMgmtService eventService = oimClient.getEventMgmtSvc();

		try {
			eventService.createUser(Long.parseLong(reconObject.getReconKey()),
					null, "FuzzyMatchEngine", "Created as New");
			System.out.println("Create New User for  Recon Key:"
					+ reconObject.getReconKey() + " Completed.");
			return true;
		} catch (Exception e) {
			System.out.println("Link Exception" + e.getMessage());
			e.printStackTrace();

		}

		return false;

	}

	public boolean linkToExistingUser(Reconciliation reconObject) {
		System.out.println("Link To Existing User for Recon Key:"
				+ reconObject.getReconKey());
		System.out.println("Link To Existing User for Recon Action:"
				+ reconObject.getReconAction());
		System.out.println("serviceProperties:" + serviceProperties);
		System.out.println("serviceProperties:"
				+ serviceProperties.getOimProviderUrl());
		System.out.println("serviceProperties:"
				+ serviceProperties.getOimAdmUsr());
		System.out.println("serviceProperties:"
				+ serviceProperties.getOimAdmPwd());
		OimClient oimClient = new OimClient(serviceProperties);
		EventMgmtService eventService = oimClient.getEventMgmtSvc();

		try {
			eventService.establishUserLink(
					Long.parseLong(reconObject.getReconKey()),
					Long.parseLong(reconObject.getUserKey()), null,
					"FuzzyMatchEngine", "Matched to Existing");
			System.out.println("Link To Existing User for Recon Key:"
					+ reconObject.getReconKey() + " Completed.");
			return true;
		} catch (Exception e) {
			System.out.println("Link Exception" + e.getMessage());
			e.printStackTrace();
		}

		return false;

	}

	public boolean closeEvent(Reconciliation reconObject) {
		System.out.println("Close Event for Recon Key:"
				+ reconObject.getReconKey());
		OimClient oimClient = new OimClient(serviceProperties);
		System.out.println("serviceProperties:" + serviceProperties);
		System.out.println("serviceProperties:"
				+ serviceProperties.getOimProviderUrl());
		System.out.println("serviceProperties:"
				+ serviceProperties.getOimAdmUsr());
		System.out.println("serviceProperties:"
				+ serviceProperties.getOimAdmPwd());
		ReconOperationsService reconService = oimClient.getReconSvc();

		try {
			reconService.closeReconciliationEvent(Long.parseLong(reconObject
					.getReconKey()));
			System.out.println("Close Event for Recon Key:"
					+ reconObject.getReconKey() + " Completed.");
			return true;
		} catch (tcAPIException e) {
			System.out.println("Link Exception" + e.getMessage());
			e.printStackTrace();
		}

		return false;

	}
}