package com.aptecllc.oim.services;

//~--- non-JDK imports --------------------------------------------------------

//import com.aptecllc.iam.selfsvc.services.OIMClient;
import Thor.API.tcResultSet;
import Thor.API.Exceptions.tcAPIException;
import Thor.API.Exceptions.tcColumnNotFoundException;
import Thor.API.Exceptions.tcInvalidLookupException;
import Thor.API.Operations.tcLookupOperationsIntf;

import com.aptecllc.iam.oim.utils.notificationprovider.APTECEMAILNotificationProvider;
import com.aptecllc.iam.oim.utils.notificationprovider.APTECEMAILPayload;
import com.aptecllc.oim.controller.OIMRESTController;
import com.aptecllc.oim.exception.UserException;
import com.aptecllc.oim.exception.UserNotFoundException;
import com.aptecllc.oim.property.ServiceProperty;
import com.aptecllc.oim.util.v2.OimClient;
import com.thortech.xl.vo.Constants;

import oracle.iam.identity.exception.NoSuchUserException;
import oracle.iam.identity.exception.OrganizationManagerException;
import oracle.iam.identity.exception.SearchKeyNotUniqueException;
import oracle.iam.identity.exception.UserAlreadyExistsException;
import oracle.iam.identity.exception.UserCreateException;
import oracle.iam.identity.exception.UserEnableException;
import oracle.iam.identity.exception.UserLookupException;
import oracle.iam.identity.exception.UserManagerException;
import oracle.iam.identity.exception.UserModifyException;
import oracle.iam.identity.exception.UserSearchException;
import oracle.iam.identity.exception.ValidationFailedException;
import oracle.iam.identity.orgmgmt.api.OrganizationManager;
import oracle.iam.identity.orgmgmt.vo.Organization;
import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.identity.usermgmt.api.UserManagerConstants;
import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.identity.usermgmt.vo.UserManagerResult;
import oracle.iam.notification.api.NotificationService;
import oracle.iam.notification.exception.EventException;
import oracle.iam.notification.exception.MultipleTemplateException;
import oracle.iam.notification.exception.NotificationException;
import oracle.iam.notification.exception.NotificationManagementException;
import oracle.iam.notification.exception.NotificationResolverNotFoundException;
import oracle.iam.notification.exception.TemplateNotFoundException;
import oracle.iam.notification.exception.UnresolvedNotificationDataException;
import oracle.iam.notification.exception.UserDetailsNotFoundException;
import oracle.iam.notification.template.TemplateService;
import oracle.iam.notification.vo.NotificationEvent;
import oracle.iam.notification.vo.NotificationTemplate;
import oracle.iam.passwordmgmt.api.PasswordMgmtService;
import oracle.iam.passwordmgmt.vo.ValidationResult;
import oracle.iam.passwordmgmt.vo.rules.PasswordRuleDescription;
import oracle.iam.platform.OIMClient;
import oracle.iam.platform.Platform;
import oracle.iam.platform.authz.exception.AccessDeniedException;
import oracle.iam.platform.entitymgr.EntityManager;
import oracle.iam.platform.entitymgr.vo.SearchCriteria;
import oracle.iam.selfservice.exception.AuthSelfServiceException;
import oracle.iam.selfservice.exception.ChangePasswordException;
import oracle.iam.selfservice.exception.InvalidLookupException;
import oracle.iam.selfservice.exception.NumberOfChallengesMismatchException;
import oracle.iam.selfservice.exception.PasswordIncorrectException;
import oracle.iam.selfservice.exception.PasswordMismatchException;
import oracle.iam.selfservice.exception.PasswordPolicyException;
import oracle.iam.selfservice.exception.PasswordResetAttemptsExceededException;
import oracle.iam.selfservice.exception.QuestionsNotDefinedException;
import oracle.iam.selfservice.exception.SetChallengeValueException;
import oracle.iam.selfservice.exception.UserAccountDisabledException;
import oracle.iam.selfservice.exception.UserAccountInvalidException;
import oracle.iam.selfservice.exception.UserAlreadyLoggedInException;
import oracle.iam.selfservice.self.selfmgmt.api.AuthenticatedSelfService;
import oracle.iam.selfservice.uself.uselfmgmt.api.UnauthenticatedSelfService;

//import com.aptecllc.oim.services.APTECEMAILNotificationProvider1;
//import com.aptecllc.iam.oim.utils.notificationprovider.APTECEMAILNotificationProvider1;
import com.aptecllc.iam.oim.utils.notificationprovider.APTECEMAILPayload;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//~--- JDK imports ------------------------------------------------------------


















import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.security.auth.login.LoginException;

/**
 * Class description
 *
 *
 * @version        1.0, 13/11/06
 * @author         Eric A. Fisher, APTEC LLC    
 */
@Service
public class OIMUserServices {
    private static final Logger logger = LoggerFactory.getLogger(OIMRESTController.class);
    @Autowired
    private ServiceProperty     serviceProperties;
    
    public Map getLookupValues(String lookupname)
    {
    	OimClient   oimClient = new OimClient(serviceProperties);
    	Map<String,String> lookupPairs= new HashMap<String,String>();
    	String lookupCode = "";
    	String lookupValue = "";
        try {
			
        	tcLookupOperationsIntf lookup    = oimClient.getTcLookupOpInt();
			
			tcResultSet rs= lookup.getLookupValues(lookupname);
			int count=rs.getRowCount();
			System.out.println("Count is->"+count);
			
		           
		            if ((rs != null) && (rs.getRowCount() > 0)) 
		            {
		                for (int i = 0; i < rs.getRowCount(); i++) 
		                {
		                	rs.goToRow(i);
		                     lookupCode = rs.getStringValue("LKV_ENCODED");
		                     lookupValue =rs.getStringValue("LKV_DECODED");
		                    
		                     lookupPairs.put(lookupValue, lookupValue);
		                    
		                }
		            }
		        
			
		} catch (tcAPIException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (tcInvalidLookupException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (tcColumnNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return lookupPairs;
    }

    /**
     * Method description
     *
     *
     * @param fieldName
     * @param value
     * @param attributesToReturn
     *
     * @return
     */
    public User getUserByIdentifier(String fieldName, String value, Set attributesToReturn) throws UserException {
        User        user      = null;
        OimClient   oimClient = new OimClient(serviceProperties);
        //tcLookupOperationsIntf lookup= oimClient.getService(tcLookupOperationsIntf.class);
        
        UserManager usrMgr    = oimClient.getUsrMgr();
        

        logger.debug("Attributes to Return: " + attributesToReturn.toString());

        try {
            user = usrMgr.getDetails(fieldName, value, attributesToReturn);
        } catch (NoSuchUserException e) {

            logger.error(e.getMessage());
            throw new UserNotFoundException("Not Found");
        } catch (UserLookupException e) {
        	logger.error(e.getMessage());
        	throw new UserException("Lookup Error");
       
        } catch (SearchKeyNotUniqueException e) {

            logger.error(e.getMessage());
            throw new UserException("Duplicates Found");
        } catch (AccessDeniedException e) {

          
            
            logger.error(e.getMessage());
            throw new UserException("Access Denied");
        }

        return user;
    }

    /**
     * Method description
     *
     *
     * @param searchCriteria
     * @param attributesToReturn
     *
     * @return
     */
    public List searchUsers(SearchCriteria searchCriteria, Set attributesToReturn) throws UserException{
        List foundUsers = null;
        OimClient   oimClient = new OimClient(serviceProperties);
        UserManager usrMgr    = oimClient.getUsrMgr();
      
        
        try {
			foundUsers = usrMgr.search(searchCriteria, attributesToReturn, null);
		} catch (UserSearchException e) {
			
			e.printStackTrace();
			throw new UserException(e);
		} catch (AccessDeniedException e) {
			
			e.printStackTrace();
			throw new UserException(e);
		}

        return foundUsers;
    }

    /**
     * Method description
     *
     *
     * @param user
     * @param orgName
     *
     * @return
     *
     * @throws UserException
     */
    public String createUser(User user, String orgName) throws UserException {
        String      response  = null;
        OimClient   oimClient = new OimClient(serviceProperties);
        UserManager usrMgr    = oimClient.getUsrMgr();

        try {
            user.setAttribute("act_key", getOrgKeyforOrganization(orgName));
            logger.error(user.toString());

            UserManagerResult result = usrMgr.create(user);

            if (result.getStatus().equals("COMPLETED")) {
                response = result.getEntityId();
            }
        } catch (ValidationFailedException e) {

            // TODO Auto-generated catch block
            response = "INVALID";
            logger.error(e.getMessage());

            throw new UserException(e.getMessage());
        } catch (UserAlreadyExistsException e) {

            // TODO Auto-generated catch block
            response = "EXISTS";
            logger.error(e.getMessage());

            throw new UserException(e.getMessage());
        } catch (UserCreateException e) {

            // TODO Auto-generated catch block
            response = "ERROR";
            logger.error(e.getMessage());

            throw new UserException(e.getMessage());
        } catch (AccessDeniedException e) {

            // TODO Auto-generated catch block
            response = "ERROR";
            logger.error(e.getMessage());

            throw new UserException(e.getMessage());
        }

        return response;
    }
    //
    /**
     * Method description
     *
     *
     * @param user
     * @param orgName
     *
     * @return
     *
     * @throws UserException
     */
    public String enableUser(String struser) throws UserException {
    	System.out.println("Inside Enable User and the user is"+struser);
        String      response  = null;
        OimClient   oimClient = new OimClient(serviceProperties);
        UserManager usrMgr    = oimClient.getUsrMgr();

        try {
            //user.setAttribute("act_key", getOrgKeyforOrganization(orgName));
            //logger.error(user.toString());

            UserManagerResult result = usrMgr.enable(struser, true);
            System.out.println("Inside Enable User and the result is"+result.getStatus()+"entity id"+result.getEntityId());

            if (result.getStatus().equals("COMPLETED")) {
                response = "SUCCESS";
            }
        } catch (ValidationFailedException e) {

            // TODO Auto-generated catch block
            response = "INVALID";
            logger.error(e.getMessage());

            throw new UserException(e.getMessage());
        } catch (AccessDeniedException e) {

            // TODO Auto-generated catch block
            response = "ERROR";
            logger.error(e.getMessage());

            throw new UserException(e.getMessage());
        } catch (UserEnableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchUserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        return response;
    }

    //
    /**
     * Method description
     *
     *
     * @param attributeMap
     *
     * @return
     *
     * @throws UserException
     */
    public String modifyUserProfile(HashMap attributeMap) throws UserException {
        OimClient                oimClient = new OimClient(serviceProperties);
        AuthenticatedSelfService selfOps   = oimClient.getAuthSelfSvc();

        try {
            selfOps.modifyProfileDetails(attributeMap);
        } catch (oracle.iam.selfservice.exception.ValidationFailedException e) {
            System.out.println("\n\n-------------------->Validation Failed exception in UserServices\n\n");
        	e.printStackTrace();
            throw new UserException(e.getMessage());
        } catch (AccessDeniedException e) {
        	System.out.println("\n\n-------------------->Access Denied exception in UserServices\n\n");
            e.printStackTrace();

            throw new UserException(e.getMessage());
        } catch (AuthSelfServiceException e) {
        	System.out.println("\n\n-------------------->User Exception exception in UserServices\n\n");
            e.printStackTrace();

            throw new UserException(e.getMessage());
        }

        return "OK";
    }

    /**
     * Method description
     *
     *
     * @param user
     * @param keyField
     * @param keyValue
     *
     * @return
     *
     * @throws UserException
     */
    public String modifyUser(User user, String keyField, String keyValue) throws UserException {
        OimClient   oimClient  = new OimClient(serviceProperties);
        UserManager usrMgr     = oimClient.getUsrMgr();
        String      returnCode = null;

        try {
        	System.out.println("\n>>>>>>>>>>>>>>>>>>>>>>Reached modifyUser in services");
            UserManagerResult result = usrMgr.modify(keyField, keyValue, user);

            if (result.getStatus().equals("COMPLETED")) {
                returnCode = "OK";
            }
        } catch (ValidationFailedException e) {
        	System.out.println("\n>>>>>>>>>>>>>>>>>>>>>>Validation Failed");
            e.printStackTrace();
        } catch (UserModifyException e) {

        	System.out.println("\n>>>>>>>>>>>>>>>>>>>>>>Modify Failed");
            e.printStackTrace();
        } catch (NoSuchUserException e) {

        	System.out.println("\n>>>>>>>>>>>>>>>>>>>>>>No Such Failed");
            e.printStackTrace();
        } catch (SearchKeyNotUniqueException e) {

        	System.out.println("\n>>>>>>>>>>>>>>>>>>>>>NotUnique Failed");
            e.printStackTrace();
        } catch (AccessDeniedException e) {

        	System.out.println("\n>>>>>>>>>>>>>>>>>>>>>>Access Denied Failed");
            e.printStackTrace();
        }

        return returnCode;
    }

    /**
     * Method description
     *
     *
     * @param userLogin
     *
     * @return
     *
     * @throws UserException
     */
    public List getUserChallenges(String userLogin) throws UserException {
        ArrayList<String>          userChallenges = new ArrayList<String>();
        OimClient                  oimClient      = new OimClient(serviceProperties);
        UnauthenticatedSelfService selfOps        = oimClient.getUnAuthSelfSvc();

        try {
            String[] challenges = selfOps.getChallengeQuestions(userLogin);

            userChallenges.addAll(Arrays.asList(challenges));
        } catch (UserAccountDisabledException e) {
           // e.printStackTrace();

            throw new UserException("User Disabled");
        } catch (UserAccountInvalidException e) {
            e.printStackTrace();

            throw new UserNotFoundException("User Invalid");
        } catch (AuthSelfServiceException e) {
            e.printStackTrace();
            if (e.getMessage().equalsIgnoreCase("User challenge questions have not been set.")) {
            	throw new UserException("Questions not Set.");
            }
            		
            throw new UserException("Self Service Exception");
        }

        return userChallenges;
    }

    /**
     * Method description
     *
     *
     * @return
     *
     * @throws UserException
     */
    public List getUserChallenges() throws UserException {
        ArrayList<String>        userChallenges = new ArrayList<String>();
        OimClient                oimClient      = new OimClient(serviceProperties);
        AuthenticatedSelfService selfOps        = oimClient.getAuthSelfSvc();

        try {
            HashMap challenges = selfOps.getChallengeValuesForSelf();

            userChallenges.addAll(challenges.keySet());
        } catch (UserAccountDisabledException e) {
            e.printStackTrace();

            throw new UserException("User Disabled");
        } catch (UserAccountInvalidException e) {
            e.printStackTrace();

            throw new UserNotFoundException("User Invalid");
        } catch (AuthSelfServiceException e) {
            e.printStackTrace();
            if (e.getMessage().equalsIgnoreCase("User challenge questions have not been set.")) {
            	throw new UserNotFoundException("Questions not Set");
            }

            throw new UserException("Self Service Exception");
        }

        return userChallenges;
    }

    // User Authenticated Service

    /**
     * Method description
     *
     *
     * @param questionMap
     *
     * @throws UserException
     */
    public void setUserChallenges(HashMap questionMap) throws UserException {
        OimClient                oimClient = new OimClient(serviceProperties);
        AuthenticatedSelfService selfOps   = oimClient.getAuthSelfSvc();

        try {
            selfOps.setChallengeValues(questionMap);
        } catch (oracle.iam.selfservice.exception.ValidationFailedException e) {
            e.printStackTrace();

            throw new UserException(e.getMessage());
        } catch (SetChallengeValueException e) {
            e.printStackTrace();

            throw new UserException(e.getMessage());
        }
    }

    // User Authenticated Service

    /**
     * Method description
     *
     *
     * @param oldPassword
     * @param newPassword
     * @param confirmPassword
     *
     * @throws UserException
     */
    public void changePasswordAsUser(String oldPassword, String newPassword, String confirmPassword)
            throws UserException {
        OimClient                oimClient = new OimClient(serviceProperties);
        AuthenticatedSelfService selfOps   = oimClient.getAuthSelfSvc();

        try {
            selfOps.changePassword(oldPassword.toCharArray(), newPassword.toCharArray(), confirmPassword.toCharArray());
        } catch (oracle.iam.selfservice.exception.ValidationFailedException e) {
            e.printStackTrace();

            throw new UserException(e.getMessage());
        } catch (ChangePasswordException e) {
            e.printStackTrace();

            throw new UserException(e.getMessage());
        }
    }

    /**
     * Method description
     *
     *
     * @param userName
     * @param questions
     * @param newPassword
     *
     * @return
     *
     * @throws UserException
     */
    public boolean resetPasswordWithChallenges(String userName, HashMap questions, String newPassword)
            throws UserException {
        OimClient                  oimClient = new OimClient(serviceProperties);
        UnauthenticatedSelfService selfOps   = oimClient.getUnAuthSelfSvc();
        boolean                    result    = false;

        try {
            result = selfOps.resetPassword(userName, questions, newPassword.toCharArray());
        } catch (UserAccountDisabledException e) {
            e.printStackTrace();

            throw new UserException("User Disabled");
        } catch (UserAccountInvalidException e) {
            e.printStackTrace();

            throw new UserException("User Invalid");
        } catch (NumberOfChallengesMismatchException e) {
            e.printStackTrace();

            throw new UserException("Incorrect Answers");
        } catch (QuestionsNotDefinedException e) {
            e.printStackTrace();

            throw new UserException("No Questions Defined");
        } catch (PasswordIncorrectException e) {
            e.printStackTrace();

            throw new UserException("Bad Password");
        } catch (PasswordMismatchException e) {
            e.printStackTrace();

            throw new UserException("Password Mismatch");
        } catch (PasswordPolicyException e) {
            e.printStackTrace();

            throw new UserException("Password Policy Failed");
        } catch (PasswordResetAttemptsExceededException e) {
            e.printStackTrace();

            throw new UserException("Reset Limit Exceeded");
        } catch (UserAlreadyLoggedInException e) {
            e.printStackTrace();

            throw new UserException("Already Logged In");
        } catch (AuthSelfServiceException e) {
            e.printStackTrace();

            throw new UserException("Self Service Exception");
        }

        return result;
    }

    /**
     * Method description
     *
     *
     * @param userName
     * @param newPassword
     *
     * @return
     *
     * @throws UserException
     */
    public boolean setUserPassword(String userName, String newPassword, boolean forceChange) throws UserException {
        OimClient   oimClient = new OimClient(serviceProperties);
        UserManager usrMgr    = oimClient.getUsrMgr();

        try {
            usrMgr.changePassword(userName, newPassword.toCharArray(), true, null, forceChange, false);
        } catch (NoSuchUserException e) {
            e.printStackTrace();

            throw new UserNotFoundException("User Invalid");
        } catch (UserManagerException e) {

            // TODO Auto-generated catch block
            e.printStackTrace();

            throw new UserException("Self Service Exception");
        } catch (AccessDeniedException e) {

            // TODO Auto-generated catch block
            e.printStackTrace();

            throw new UserException("Self Service Exception");
        }

        return true;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public List getSystemChallenges() {
        ArrayList<String>          sysChallenges = new ArrayList<String>();
        OimClient                  oimClient     = new OimClient(serviceProperties);
        UnauthenticatedSelfService selfOps       = oimClient.getUnAuthSelfSvc();

        try {
            String[] challenges = selfOps.getSystemChallengeQuestions();

            sysChallenges.addAll(Arrays.asList(challenges));
        } catch (InvalidLookupException e) {

            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (AuthSelfServiceException e) {

            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return sysChallenges;
    }

    /**
     * Method description
     *
     *
     * @param orgName
     *
     * @return
     */
    public Long getOrgKeyforOrganization(String orgName) {
        OimClient           oimClient   = new OimClient(serviceProperties);
        OrganizationManager orgMgr      = oimClient.getOrgManager();
        Long                orgKey      = null;
        Set<String>         returnAttrs = new HashSet<String>();

        returnAttrs.add("act_key");
        returnAttrs.add(oracle.iam.identity.orgmgmt.api.OrganizationManagerConstants.AttributeName.ID_FIELD.getId());
        returnAttrs.add(oracle.iam.identity.orgmgmt.api.OrganizationManagerConstants.AttributeName.ORG_NAME.getId());

        Organization org = null;

        try {
            org    = orgMgr.getDetails(orgName, returnAttrs, true);
            orgKey = (Long) org.getAttribute(
                oracle.iam.identity.orgmgmt.api.OrganizationManagerConstants.AttributeName.ID_FIELD.getId());
        } catch (OrganizationManagerException e) {

            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (AccessDeniedException e) {

            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return orgKey;
    }

    /**
     * Method description
     *
     *
     * @param userName
     * @param password
     *
     * @return
     */
    public String validatePassword(String userName, char[] password) {
        OimClient           oimClient = new OimClient(serviceProperties);
        PasswordMgmtService pwdSvc    = oimClient.getPwdMgr();
        ValidationResult    results   = pwdSvc.validatePasswordAgainstPolicy(password, userName, Locale.US);

        if (results.isPasswordValid()) {
            return "Valid";
        } else {
            StringBuffer                  failedRules   = new StringBuffer();
            List<PasswordRuleDescription> rulesViolated =
                results.getPolicyViolationsDescription().getPasswordRulesDescription();

            for (PasswordRuleDescription rule : rulesViolated) {
                failedRules.append(rule.getDisplayValue()).append("|");
            }

            return failedRules.toString();
        }
    }
    
    /**
     * 	Two field User update function, result of difficulty with random errors with others
     * 	-Dan Donnelly - Sprint 8: 12/10/2015
     * 
     * @param HashMap of attributes
     * @param String of user login
     * 
     * @return String for OK code
     * 
     * 
     */
    public String updateUser(HashMap<String, Object> attrs, String login){
    	OimClient   oimClient  = new OimClient(serviceProperties);
        UserManager usrMgr     = oimClient.getUsrMgr();
         
        User retUser = new User(login, attrs);
        System.out.println("\n\n>>>>>>>>>>User to Modify"+retUser.toString());
        UserManagerResult res = null;
        try {
			res = usrMgr.modify(UserManagerConstants.AttributeName.USER_LOGIN.getId(), login, retUser);
		} catch (ValidationFailedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UserModifyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchUserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SearchKeyNotUniqueException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (AccessDeniedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	System.out.println("\n\n>>>>>>>>>>>>>>>>>>UserMgr Result: "+res);
        if(res!=null)
        	return "OK";
        else
        	return "NOT OK";
    }
    
    // Added on 08/17
    
    //sendAccountClaimingEmail(templateName, altEmail, firstName, lastName, userLogin, usermanager);
		
		public void sendAccountClaimingEmail(String templateName, String altEmail, String firstName, String lastName, String userLogin) throws NoSuchUserException, UserLookupException, AccessDeniedException {
			
			//Declare variables for method
			OimClient   oimClient  = new OimClient(serviceProperties);
	        UserManager usrMgr     = oimClient.getUsrMgr();
	        
	        
	      
			//Platform.getService(EntityManager.class);
	 		
	        NotificationService notifServ = oimClient.getNotificationMgr();
	        
	        //NotificationService templateserv    = oimClient.getTemplateMgr();
	       // System.out.println("templateserv------------------------------"+templateserv);
	        NotificationTemplate template = null;
	       Locale defaultLocale = new Locale("en", "US");
	        try {
				template = notifServ.lookupTemplate("AccountClaimingEmail", defaultLocale);
				
				
			} catch (TemplateNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (MultipleTemplateException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (NotificationManagementException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	 		//UserManager usrMgr = Platform.getService(UserManager.class);
	 		NotificationEvent accountClaimingNotificationEvent = null;
	 
	 		//Use params to call createNotificationEvent method
	 		System.out.println("Before accountClaimingNotificationEvent");
	 		//if alternate email is not null
	 		//if usermanager key is null
	 		
	 		System.out.println("Just before sending email altEmail and managerKey are-"+altEmail+"-");	
	 		
	 		
	 		
	 		
	 		
	 		accountClaimingNotificationEvent = createNotificationEvent(notifServ,"AccountClaimingEmail","joysgrace@gmail.com", firstName, lastName, userLogin);
	 		
	 		System.out.println("After accountClaimingNotificationEvent");
	
	 		
	 		//Attempt to send e-mail with the given params. 
	 		try {
	 			System.out.println("Attempting to send email.");
					notifServ.notify(accountClaimingNotificationEvent);
					System.out.println("After sending attempt.");
					//After e-mail is sent,	the "email sent" attribute must be toggled to "Y", so another e-mail is not sent			
					System.out.println("Now check EMAILSENT to 'Y'");
					HashMap<String, Object>  updateAttrs = new HashMap<String, Object>();
					updateAttrs.put("EmailSent", "Y");
					User user = new User(userLogin, updateAttrs);
					//usrMgr.modify(UserManagerConstants.AttributeName.USER_LOGIN.getId(), userLogin, user);
					
					
					
				//Catching a whole mess of things that can go wrong. With print lines so you know which one it is.	
				} catch (UserDetailsNotFoundException e) {
					System.out.println("UserDetailsNotFoundException");
					e.printStackTrace();
				} catch (EventException e) {
					e.printStackTrace();
					System.out.println("EventException");
				} catch (UnresolvedNotificationDataException e) {
					e.printStackTrace();
					System.out.println("UnresolvedNotificationDataException");
				} catch (TemplateNotFoundException e) {
					e.printStackTrace();
					System.out.println("TemplateNotFoundException");
				} catch (MultipleTemplateException e) {
					e.printStackTrace();
					System.out.println("MultipleTemplateException");
				} catch (NotificationResolverNotFoundException e) {
					e.printStackTrace();
					System.out.println("NotificationResolverNotFoundException");
				} catch (NotificationException e) {
					e.printStackTrace();
					System.out.println("NotificationException");
				} catch (AccessDeniedException e) {
					e.printStackTrace();
					System.out.println("AccessDeniedException");
				}
	 		
				}


//

public NotificationEvent createNotificationEvent(NotificationService notifServ,String templateName, String altEmail, String firstName, String lastName, String userLogin) throws NoSuchUserException,  AccessDeniedException {
		
		
		//Declare variables and load your dynamic variables into a HashMap to be sent to he Notification Event
		NotificationEvent event = new NotificationEvent();
       //System.out.println("Template Name: "+templateName);
       
		event.setTemplateName("AccountClaimingEmail");
		//event.getTemplateName();
		System.out.println("template set in the event is"+event.getTemplateName());
       HashMap<String, Object> map = new HashMap<String, Object>();
       map.put("UserFirstName", firstName);
       map.put("UserLastName", lastName);
       
       //hashedkey uses the encrypt function to create a long hashed key of the userlogin, to put it in the notification event.
       BigInteger hashedkey = encrypt((String) userLogin);
       map.put("userkey", hashedkey);
       
       //Set all elements of the map to the notification event.
       event.setParams(map);
       
       //Some APTEC packages used here to sent the email.
       List<String> emailToList = new ArrayList<String>();
       emailToList.add("joysgrace@gmail.com");
       
       //Deciding whether or not to send the email to the alternate email address or to the manager's email address, based upon
       //the template that is being sent. 
       
       
       if(templateName.equals("AccountClaimingEmail")){
       	System.out.println("Sending template: "+templateName);
       	emailToList.add("joysgrace@gmail.com");
       }
       
       if(templateName.equals("AccountClaimingEmailWinthrop")){
       	System.out.println("Sending template: "+templateName);
       	emailToList.add(altEmail);
       }
       
       if(templateName.equals("AccountClaimingEmailEmployee") ){
       	
           //System.out.println("Email being sent to manager: "+managerEmail+", with template: "+templateName);
           emailToList.add(altEmail);
       }
       
       //Finish off setting up the e-mail notification event, then return the event to be sent.
       
       System.out.println("Before creating emailPayload");
      
       APTECEMAILPayload emailPayload = new APTECEMAILPayload(emailToList, "oimadmin@nyumc.org", true, "UTF-8");
       System.out.println("After creating emailPayload");
       
       //Creating a new list to house the CC email addresses)
       List<String> ccEmails = new ArrayList<String>();
       ccEmails.add("NYUSecAdminTeam@nyumc.org");
       
       //Putting the from email address and CC email list into the payload to be sent
       System.out.println("Before setting senderemailID in emailPayload");
       emailPayload.setSenderEmailID("oimadmin@nyumc.org");
       System.out.println("After setting senderemailID in emailPayload");
       System.out.println("Before setting ccemailID in emailPayload");
       emailPayload.setCcMailIds(ccEmails);
       System.out.println("After setting ccemailID in emailPayload");
       //emailPayload.setBccEmailIds(<OPTIONAL-LISTOFBCCADDRESSES>);
      
      
       System.out.println("Before creating APTECEMAILNotificationProvider");
       
       OimClient   oimClient = new OimClient(serviceProperties);
       //tcLookupOperationsIntf lookup= oimClient.getService(tcLookupOperationsIntf.class);
       System.out.println("oimClient"+oimClient);
       
       //TemplateService templateserv    = oimClient.getTemplateMgr();
       //System.out.println("templateserv"+templateserv);
       //APTECEMAILNotificationProvider1 notProvider = new APTECEMAILNotificationProvider1(notifServ,serviceProperties,event, emailPayload, "http://iam1-soa.nyumc.org:8001/ucs/messaging/webservice");
       System.out.println("After creating APTECEMAILNotificationProvider");
       try {
			//if (notProvider.sendMessage())
				System.out.println("Email Sent Successfully to User with Email Address");
			} catch (Exception e) {
				e.printStackTrace();
			}
       
       return event;
	}
   


static BigInteger p = new BigInteger(
       "186903683994784883974286803981340343409");
static BigInteger q = new BigInteger(
       "238365206090827088390448048852555109291");
static BigInteger n = p.multiply(q);
static BigInteger phiN = (p.subtract(BigInteger.ONE)).multiply((q
       .subtract(BigInteger.ONE)));
static BigInteger e = new BigInteger("449");
static BigInteger d = new BigInteger(
       "17364106129279623304364236508747275879893306787892962132847318388210189166049");
public static BigInteger encodeMessageAsBigInteger(String plaintext) {
return new BigInteger(plaintext.getBytes());
}
    
public static String recoverTextFromBigInteger(BigInteger plaintext) {
return new String(plaintext.toByteArray());
}
    
private static BigInteger encrypt(String input){
    
     BigInteger m = encodeMessageAsBigInteger(input);
    
     BigInteger c = m.modPow(e, n);
     return c;
     }
    
private static BigInteger encryptInt(int input){
    
 String aString = Integer.toString(input);
 
     BigInteger m = encodeMessageAsBigInteger(aString);
    
     BigInteger c = m.modPow(e, n);
     return c;
     }
    
private static BigInteger decrypt(BigInteger c){
return c.modPow(d,n);
}

    //

}
