package com.aptecllc.oim.vo;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "result")
public class NewInnovationsSearchResult {

	private NewInnovationsPerson person;
	private Boolean exactMatch;
	
	public NewInnovationsSearchResult(){}
	
	public NewInnovationsSearchResult(NewInnovationsPerson person, Boolean exactMatch){
		this.setPerson(person);
		this.setExactMatch(exactMatch);
	}

	public Boolean getExactMatch() {
		return exactMatch;
	}

	public void setExactMatch(Boolean exactMatch) {
		this.exactMatch = exactMatch;
	}

	public NewInnovationsPerson getPerson() {
		return person;
	}

	public void setPerson(NewInnovationsPerson person) {
		this.person = person;
	}

}
