package com.aptecllc.oim.vo;

import java.util.ArrayList;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="results")
public class NewInnovationsSearchResultList {

	private List<NewInnovationsSearchResult> results;
	private int count;
	
	public NewInnovationsSearchResultList(){
		results= new ArrayList<NewInnovationsSearchResult>();
		count = 0;
	}
	
	public int add(NewInnovationsSearchResult newResult){
		results.add(newResult);
		count++;
		return count;
	}
	
	@XmlElement(name="result")
	public List<NewInnovationsSearchResult> getResults(){
		return results;
	}
	
	public int getCount(){
		return count;
	}
}
