package com.aptecllc.oim.vo;

//~--- JDK imports ------------------------------------------------------------

import java.io.Serializable;

/**
 * Class description
 *
 *
 * @version        1.0, 14/04/06
 * @author         Eric A. Fisher, APTEC LLC    
 */
public class Request implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String            justification;
    private Object            requestObject;
    private String			  requestingUser;

    public String getRequestingUser() {
		return requestingUser;
	}

	public void setRequestingUser(String requestingUser) {
		this.requestingUser = requestingUser;
	}

	/**
     * Method description
     *
     *
     * @return
     */
    public String getJustification() {
        return justification;
    }

    /**
     * Method description
     *
     *
     * @param justification
     */
    public void setJustification(String justification) {
        this.justification = justification;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public Object getRequestObject() {
        return requestObject;
    }

    /**
     * Method description
     *
     *
     * @param requestObject
     */
    public void setRequestObject(Object requestObject) {
        this.requestObject = requestObject;
    }
}
