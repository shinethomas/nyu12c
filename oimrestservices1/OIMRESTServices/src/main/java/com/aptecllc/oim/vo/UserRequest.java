package com.aptecllc.oim.vo;

import java.io.Serializable;

/**
 * Class description
 *
 *
 * @version        1.0, 14/04/06
 * @author         Eric A. Fisher, APTEC LLC    
 */
public class UserRequest implements Serializable {
    private static final long serialVersionUID = 1L;
    private String            justification;
    private UserAccount       requestObject;
    private String            requestingUser;

    /**
     * Method description
     *
     *
     * @return
     */
    public String getJustification() {
        return justification;
    }

    /**
     * Method description
     *
     *
     * @param justification
     */
    public void setJustification(String justification) {
        this.justification = justification;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public UserAccount getRequestObject() {
        return requestObject;
    }

    /**
     * Method description
     *
     *
     * @param requestObject
     */
    public void setRequestObject(UserAccount requestObject) {
        this.requestObject = requestObject;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getRequestingUser() {
        return requestingUser;
    }

    /**
     * Method description
     *
     *
     * @param requestingUser
     */
    public void setRequestingUser(String requestingUser) {
        this.requestingUser = requestingUser;
    }
}
