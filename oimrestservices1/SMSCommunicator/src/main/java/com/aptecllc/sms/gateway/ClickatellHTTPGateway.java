package com.aptecllc.sms.gateway;

//~--- non-JDK imports --------------------------------------------------------

import com.aptecllc.sms.message.AbstractSMSMessage;
import com.aptecllc.sms.message.ClickatellSMSMessage;

import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

/**
 * Gateway Client for Clickatells HTTP SMS Gateway
 * Set Properties api_id, username, password, and senderID with their correct
 * values at bean injection.
 *
 *
 * @version        1.0, 14/04/22
 * @author         Eric A. Fisher, APTEC LLC
 */
public class ClickatellHTTPGateway implements ISMSGateway {
    private static final String API_URL = "https://api.clickatell.com/http/sendmsg";
    
    private String        api_id;
    private String        username;
    private String        password;
    private String        senderID;
    RestTemplate          restTemplate;

    public ClickatellHTTPGateway() {
        super();
        restTemplate = new RestTemplate();
    }

    /**
     * @return the api_id
     */
    public String getApi_id() {
        return api_id;
    }

    /**
     * @param api_id the api_id to set
     */
    public void setApi_id(String api_id) {
        this.api_id = api_id;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the senderID
     */
    public String getSenderID() {
        return senderID;
    }

    /**
     * @param senderID the senderID to set
     */
    public void setSenderID(String senderID) {
        this.senderID = senderID;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public AbstractSMSMessage getNewMessage() {
        ClickatellSMSMessage message = new ClickatellSMSMessage();

        message.setApi_id(this.getApi_id());
        message.setUsername(this.getUsername());
        message.setPassword(this.getPassword());
        message.setSenderID(this.getSenderID());
      

        return message;
    }

    /* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ClickatellHTTPGateway [api_id=" + api_id + ", username="
				+ username + ", password=" + password + ", senderID="
				+ senderID + "]";
	}

	/**
     * Method description
     *
     *
     * @param message
     *
     * @return
     */
    public String sendMessage(AbstractSMSMessage message) {
    	String response = null;
    	
        try {
             response= restTemplate.postForObject(API_URL, message.getTransmittalObject(), String.class);
            System.out.println(response);
        } catch (HttpClientErrorException hce) {
            return "ERR";
        }

        return response;
    }
}
