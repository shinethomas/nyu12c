package com.aptecllc.sms.gateway;

import com.aptecllc.sms.message.AbstractSMSMessage;

public interface ISMSGateway {
	abstract public AbstractSMSMessage getNewMessage();
	abstract public String sendMessage(AbstractSMSMessage message);
}
