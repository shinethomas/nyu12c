package com.aptecllc.sms.gateway;

//~--- non-JDK imports --------------------------------------------------------


import com.aptecllc.sms.message.AbstractSMSMessage;
import com.aptecllc.sms.message.TwilioSMSMessage;
import com.aptecllc.spring.rest.BasicAuthInterceptor;

import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

//~--- JDK imports ------------------------------------------------------------


import java.util.Collections;

/**
 * Class description
 *
 *
 * @version        1.0, 14/06/17
 * @author         Eric A. Fisher, APTEC LLC
 */
public class TwilioSMSGateway implements ISMSGateway {
    private static final String REST_URL = "https://api.twilio.com/2010-04-01/Accounts/{AccountSid}/Messages.json";
    private String              AccountSID;
    private String              AuthToken;
    private String              SenderID;
    private RestTemplate        restTemplate;

    /**
     * Constructs ...
     *
     */
    public TwilioSMSGateway() {
        super();
        restTemplate = new RestTemplate();
    }

    /**
     * Constructs ...
     *
     *
     * @param pAccountSid
     * @param pAuthToken
     * @param pSenderID
     */
    public TwilioSMSGateway(String pAccountSid, String pAuthToken, String pSenderID) {
        super();
        this.AccountSID = pAccountSid;
        this.AuthToken  = pAuthToken;
        this.SenderID   = pSenderID;
        restTemplate    = new RestTemplate();

        ClientHttpRequestInterceptor basicAuthInterceptor = new BasicAuthInterceptor(this.AccountSID, this.AuthToken);

        restTemplate.setInterceptors(Collections.singletonList(basicAuthInterceptor));
    }

    /**
     *     @return the authToken
     */
    public String getAuthToken() {
        return AuthToken;
    }

    /**
     * @param authToken the authToken to set
     */
    public void setAuthToken(String authToken) {
        AuthToken = authToken;
    }

    /**
     * @return the senderID
     */
    public String getSenderID() {
        return SenderID;
    }

    /**
     * @param senderID the senderID to set
     */
    public void setSenderID(String senderID) {
        SenderID = senderID;
    }

    /**
     * @return the accountSID
     */
    public String getAccountSID() {
        return AccountSID;
    }

    /**
     * @param accountSID the accountSID to set
     */
    public void setAccountSID(String accountSID) {
        AccountSID = accountSID;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public AbstractSMSMessage getNewMessage() {

       TwilioSMSMessage message = new TwilioSMSMessage();
       message.setSenderID(this.SenderID);
       
       return message;
    }

    /**
     * Method description
     *
     *
     * @param message
     *
     * @return
     */
    public String sendMessage(AbstractSMSMessage message) {
        String response = null;

        try {
        	
            response = restTemplate.postForObject(REST_URL, message.getTransmittalObject(), String.class,
                    this.AccountSID);
            System.out.println(response);
        } catch (HttpClientErrorException hce) {
            return "ERR";
        }

        return response;
    }
}
