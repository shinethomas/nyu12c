package com.aptecllc.http.client;

import java.io.IOException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.support.HttpRequestWrapper;

import com.aptecllc.wls.providers.token.APITokenType;

public class TokenAuthHeaderRequestInterceptor implements
		ClientHttpRequestInterceptor {
	
	private String sharedSecret;
	private String apiKey;
	private String userToAssert;
	private APITokenType tokenType;
	
	public String getSharedSecret() {
		return sharedSecret;
	}

	public void setSharedSecret(String sharedSecret) {
		this.sharedSecret = sharedSecret;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public String getUserToAssert() {
		return userToAssert;
	}

	public void setUserToAssert(String userToAssert) {
		this.userToAssert = userToAssert;
	}

	public TokenAuthHeaderRequestInterceptor() {
		
	}
    public TokenAuthHeaderRequestInterceptor(String pApiKey, String pSharedSecret, String pUserToAssert) {
		sharedSecret = pSharedSecret;
		apiKey = pApiKey;
		userToAssert = pUserToAssert;
		
		tokenType = new APITokenType(apiKey, userToAssert);
	}

	public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
		HttpRequestWrapper requestWrapper = new HttpRequestWrapper(request);
		
		String authToken = tokenType.generateAPIToken(sharedSecret);
		
	    requestWrapper.getHeaders().set("APIToken", authToken);
	 
	    return execution.execute(requestWrapper, body);
	}

}
