package com.aptecllc.iam.selfsvc.exception;

public class UserCreateException extends Exception {

	public UserCreateException() {
		// TODO Auto-generated constructor stub
	}

	public UserCreateException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public UserCreateException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public UserCreateException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
