package com.aptecllc.iam.selfsvc.exception;

public class UserUpdateException extends Exception {

	public UserUpdateException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserUpdateException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public UserUpdateException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public UserUpdateException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
