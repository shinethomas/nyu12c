package com.aptecllc.iam.selfsvc.vo;

//~--- non-JDK imports --------------------------------------------------------

import org.springframework.binding.message.MessageBuilder;
import org.springframework.binding.message.MessageContext;
import org.springframework.binding.validation.ValidationContext;

//~--- JDK imports ------------------------------------------------------------

import java.io.Serializable;

import java.util.Date;

/**
 * Class description
 *
 *
 * @version        1.0, 14/05/12
 * @author         Eric A. Fisher, APTEC LLC
 */
public class OTP implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String            verificationCode = "";
    private String            smsNumber;
    private String            authCode;
    private Date              authCodeGeneratedTime;
    private String            personalemail;

    /**
     * Constructs ...
     *
     */
    public OTP() {}

    /**
     * @return the smsNumber
     */
    public String getSmsNumber() {
    	System.out.println("Sms Number is:"+smsNumber);
        return smsNumber;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getSmsForDisplay() {
        return smsNumber.substring(getSmsNumber().length() - 4);
    }

    /**
     * @param smsNumber the smsNumber to set
     */
    public void setSmsNumber(String smsNumber) {
        if (smsNumber.startsWith("1")) {
            this.smsNumber = smsNumber;
        } else {
            this.smsNumber = "1" + smsNumber;
        }
    }

    //--
    
    /**
     * @return the personalemail
     */
    public String getpersonalEmail() {
    	System.out.println("PersonalEmail is:"+personalemail);
        return personalemail;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getpersonalEmailForDisplay() {
    	System.out.println("personal email in OTP.java is:"+personalemail);
        return personalemail.substring(getpersonalEmail().length() - 8);
    }

    /**
     * @param smsNumber the smsNumber to set
     */
    public void setpersonalEmail(String personalemail) {
            System.out.println("inside setpersonalEmail"+personalemail);
            this.personalemail = personalemail;
        
    }

    
    //--
    
    /**
     * @return the verificationCode
     */
    public String getVerificationCode() {
        return verificationCode;
    }

    /**
     * @param verificationCode the verificationCode to set
     */
    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    /**
     * @return the authCode
     */
    public String getAuthCode() {
        return authCode;
    }

    /**
     * @param authCode the authCode to set
     */
    public void setAuthCode(String authCode) {
        this.authCode         = authCode;
        authCodeGeneratedTime = new Date();
    }

    /**
     *     @return the authCodeGeneratedTime
     */
    public Date getAuthCodeGeneratedTime() {
        return authCodeGeneratedTime;
    }

    /**
     * @param authCodeGeneratedTime the authCodeGeneratedTime to set
     */
    public void setAuthCodeGeneratedTime(Date authCodeGeneratedTime) {
        this.authCodeGeneratedTime = authCodeGeneratedTime;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return "This is your verification code " + this.authCode + ".  This code is good for 3 minutes.";
    }

    /**
     * Method description
     *
     *
     * @param context
     */
    public void validateVerifyCode(ValidationContext context) {
        MessageContext messages = context.getMessageContext();

        if ("".equals(this.verificationCode)) {
            messages.addMessage(
                new MessageBuilder().error().source("VerificationCode").defaultText(
                    "Verification code must be entered.").build());
        }

        if (!this.verificationCode.equals(this.authCode)) {
            messages.addMessage(
                new MessageBuilder().error().source("VerificationCode").defaultText(
                    "Verification code is not valid.").build());
        }
        
        Date currentTime = new Date();
        
        if (currentTime.getTime() - this.authCodeGeneratedTime.getTime() > 180000L) {
            messages.addMessage(
                    new MessageBuilder().error().source("VerificationCode").defaultText(
                        "Verification code has expired.  Select resend code for a new one.").build());
        }

       
    }
    
    /**
     * Method description
     *
     *
     * @param context
     */
    public void validateVerifyEmailCode(ValidationContext context) {
        MessageContext messages = context.getMessageContext();

        if ("".equals(this.verificationCode)) {
            messages.addMessage(
                new MessageBuilder().error().source("VerificationCode").defaultText(
                    "Verification code must be entered.").build());
        }

        if (!this.verificationCode.equals(this.authCode)) {
            messages.addMessage(
                new MessageBuilder().error().source("VerificationCode").defaultText(
                    "Verification code is not valid.").build());
        }
        
        Date currentTime = new Date();
        
        if (currentTime.getTime() - this.authCodeGeneratedTime.getTime() > 180000L) {
            messages.addMessage(
                    new MessageBuilder().error().source("VerificationCode").defaultText(
                        "Verification code has expired.  Select resend code for a new one.").build());
        }

       
    }
}
