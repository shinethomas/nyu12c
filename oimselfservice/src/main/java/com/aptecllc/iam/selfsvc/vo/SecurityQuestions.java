package com.aptecllc.iam.selfsvc.vo;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Class description
 *
 *
 * @version        1.0, 13/10/21
 * @author         Eric A. Fisher, APTEC LLC    
 */
public class SecurityQuestions implements Serializable {
    private String question1;
    private String answer1;
    private String question2;
    private String answer2;
    private String question3;
    private String answer3;

    /**
     * Constructs ...
     *
     */
    public SecurityQuestions() {

        // TODO Auto-generated constructor stub
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getQuestion1() {
        return question1;
    }

    /**
     * Method description
     *
     *
     * @param question1
     */
    public void setQuestion1(String question1) {
        this.question1 = question1;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getAnswer1() {
        return answer1;
    }

    /**
     * Method description
     *
     *
     * @param answer1
     */
    public void setAnswer1(String answer1) {
        this.answer1 = answer1;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getQuestion2() {
        return question2;
    }

    /**
     * Method description
     *
     *
     * @param question2
     */
    public void setQuestion2(String question2) {
        this.question2 = question2;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getAnswer2() {
        return answer2;
    }

    /**
     * Method description
     *
     *
     * @param answer2
     */
    public void setAnswer2(String answer2) {
        this.answer2 = answer2;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getQuestion3() {
        return question3;
    }

    /**
     * Method description
     *
     *
     * @param question3
     */
    public void setQuestion3(String question3) {
        this.question3 = question3;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getAnswer3() {
        return answer3;
    }

    /**
     * Method description
     *
     *
     * @param answer3
     */
    public void setAnswer3(String answer3) {
        this.answer3 = answer3;
    }
    
    public HashMap getQuestionMap() {
    	HashMap qaMap = new HashMap();
    	
    	if (!"".equals(question1) && question1!=null) {
    		qaMap.put(question1, answer1);
    	}
    	if (!"".equals(question2)&& question2!=null) {
    		qaMap.put(question2, answer2);
    	}
    	if (!"".equals(question3)&& question3!=null) {
    		qaMap.put(question3, answer3);
    	}
    	
    	return qaMap;
    }

	@Override
	public String toString() {
		return "SecurityQuestions [question1=" + question1 + ", answer1="
				+ answer1 + ", question2=" + question2 + ", answer2=" + answer2
				+ ", question3=" + question3 + ", answer3=" + answer3 + "]";
	}
}
