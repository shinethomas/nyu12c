package com.aptecllc.iam.selfsvc.vo;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.springframework.binding.message.MessageBuilder;
import org.springframework.binding.message.MessageContext;
import org.springframework.binding.validation.ValidationContext;

import com.aptecllc.iam.selfsvc.exception.UserNotFoundException;
import com.aptecllc.iam.selfsvc.services.OIMUserServicesClient;
import com.aptecllc.iam.selfsvc.utils.BaseSelfServiceUtilities;

public class UserAccount implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private HashMap<String, Object> mAttributes;
	private String mUniqueID;
	
	
	public UserAccount() {
		mAttributes = new HashMap<String,Object>();
	}
	
	
	public String getOrganizationName() {
		return (String)mAttributes.get("organizationName");
	}
	
	public void setOrganizationName(String orgName) {
		mAttributes.put("organizationName", orgName);
	}
	
	
	 public String getTitle() {
		return (String)mAttributes.get("title");
	}

	public void setTitle(String title) {
		mAttributes.put("title",title);
	}

	public String getEmailAddress() {
		return (String)mAttributes.get("emailAddress");
	}

	public void setEmailAddress(String emailAddress) {
		mAttributes.put("emailAddress",emailAddress);
	}

	public String getPhoneNumber() {
		return (String)mAttributes.get("phoneNumber");
	}

	public void setPhoneNumber(String phoneNumber) {
		mAttributes.put("phoneNumber",phoneNumber);
	}
	
	
	
	//

	public String getExternalaffiliation() {
		return (String)this.mAttributes.get("externalaffiliation");
	}

	public void setExternalaffiliation(String externalaffiliation) {
		this.mAttributes.put("externalaffiliation",externalaffiliation);
	}
	//
	/*
	public String getSponsorsFirstName(){
		return (String)this.mAttributes.get("SponsorsFirstName");
	}
	
	public void setSponsorsFirstName(String SponsorsFirstName) {
		this.mAttributes.put("SponsorsFirstName",SponsorsFirstName);
	}
	
	 
	 
	public String getSponsorsLastName(){
		return (String)this.mAttributes.get("SponsorsLastName");
	}
	
	public void setSponsorsLastName(String SponsorsLastName) {
		this.mAttributes.put("SponsorsLastName",SponsorsLastName);
	}
	*/
	
	// added on 5/15/2017
	
	public String getSponsorAlternateEmail() {
		return (String)this.mAttributes.get("sponsorAlternateEmail");
	}

	public void setSponsorAlternateEmail(String sponsorAlternateEmail) {
		this.mAttributes.put("sponsorAlternateEmail",sponsorAlternateEmail);
	}
	
	public String getSponsorAlternatePhone() {
		return (String)this.mAttributes.get("sponsorAlternatePhone");
	}

	public void setSponsorAlternatePhone(String sponsorAlternatePhone) {
		this.mAttributes.put("sponsorAlternatePhone",sponsorAlternatePhone);
	}
	
	public String getPersonalEmail() {
		return (String)this.mAttributes.get("personalEmail");
	}

	public void setPersonalEmail(String personalEmail) {
		this.mAttributes.put("personalEmail",personalEmail);
	}
	
	// added on 8th Dec 2017
	
	public String getSilkRoadIntegration() {
		return (String)this.mAttributes.get("silkRoadIntegration");
	}

	public void setSilkRoadIntegration(String silkRoadIntegration) {
		this.mAttributes.put("silkRoadIntegration",silkRoadIntegration);
	}
	
	
	public String getRole()
	{
		return (String)this.mAttributes.get("role");
	}
	
	public void setRole(String role) {
		this.mAttributes.put("role", role);
	}
	
	// ended on 8th Dec 2017
	
	
	/*public String getTerminateDate() {
		return (String)this.mAttributes.get("terminationDate");
	}

	public void setTerminationDate(String terminationDate) {
		this.mAttributes.put("terminationDate",terminationDate);
	}*/
	
	public String getSSN() {
		return (String)this.mAttributes.get("SSN");
	}

	public void setSSN(String SSN) {
		this.mAttributes.put("SSN",SSN);
	}
	
	public String getEmailRequired() {
		return (String)this.mAttributes.get("emailRequired");
	}

	public void setEmailRequired(String emailRequired) {
		this.mAttributes.put("emailRequired",emailRequired);
	}
	
	public String getSponsorNetID() {
		return (String)this.mAttributes.get("sponsorNetID");
	}

	public void setSponsorNetID(String sponsorNetID) {
		this.mAttributes.put("sponsorNetID",sponsorNetID);
	}
	
	public String getVendor() {
		return (String)this.mAttributes.get("vendor");
	}

	public void setVendor(String vendor) {
		this.mAttributes.put("vendor",vendor);
	}
	
	public String getSponsor() {
		return (String)this.mAttributes.get("sponsor");
	}

	public void setSponsor(String sponsor) {
		this.mAttributes.put("sponsor",sponsor);
	}
	
	public String getDOB() {
		return (String)this.mAttributes.get("DOB");
	}

	public void setDOB(String DOB) {
		System.out.println("setting value of DoB in UserAccount"+DOB);
		this.mAttributes.put("DOB",DOB);
	}
	
	
	

	
	//
	

	public String getDepartmentNumber() {
		return (String)mAttributes.get("departmentNumber");
	}

	public void setDepartmentNumber(String departmentNumber) {
		mAttributes.put("departmentNumber", departmentNumber);
	}

	public String getOfficeLocation() {
		return (String)mAttributes.get("officeLocation");
	}

	public void setOfficeLocation(String officeLocation) {
		mAttributes.put("officeLocation",officeLocation);
	}

	public String getLastName() {
		return (String) this.mAttributes.get("lastName");
	}

	public void setLastName(String lastName) {
		this.mAttributes.put("lastName",lastName);
	}

	public String getMiddleInitial() {
		return (String) this.mAttributes.get("middleInitial");
	}

	public void setMiddleInitial(String middleInitial) {
		this.mAttributes.put("middleInitial", middleInitial);
	}

	public String getPrefFirstName() {
		return (String) this.mAttributes.get("prefFirstName");
	}

	public void setPrefFirstName(String prefFirstName) {
		this.mAttributes.put("prefFirstName",prefFirstName);
	}

	public void setAttribute(String attr, Object val)
	  {
	    if (attr == null) {
	      return;
	    }

	    if (val == null) {
	      this.mAttributes.remove(attr);
	    }

	    this.mAttributes.put(attr, val);
	  }

	  public Object getAttribute(String attr)
	  {
	    if (attr == null) {
	      return null;
	    }
	    return this.mAttributes.get(attr);
	  }
	  
	  @JsonIgnore
	  public Set getAttributeNames()
	  {
	    return this.mAttributes.keySet();
	  }
	  public HashMap<String, Object> getAttributes()
	  {
	    return this.mAttributes;
	  }
	  
	  public void setAttributes(HashMap attribs)
	  {
	    mAttributes=attribs;
	  }
	  public String toString()
	  {
	    StringBuffer buff = new StringBuffer();
	    if (this.mUniqueID != null) {
	      buff.append(this.mUniqueID);
	      buff.append("\n");
	    }
	    if (this.mAttributes != null) {
	      buff.append(this.mAttributes.toString());
	    }
	    return buff.toString();
	  }

	public String getUserName() {
		return mUniqueID;
	}

	public void setUserName(String userName) {
		mUniqueID = userName;
	}
	
	public String getAccountId() {
		return (String) this.mAttributes.get("accountId");
	}
	
	public void setAccountId(String pAccountID) {
		this.mAttributes.put("accountId", pAccountID);
	}
	public String getFirstName() {
		return (String) this.mAttributes.get("firstName");
	}
	
	public void setFirstName(String pFirstName) {
		this.mAttributes.put("firstName", pFirstName);
	}
	public void setStatus(String pStatus) {
		this.mAttributes.put("status", pStatus);
		
	}
	public String getStatus() {
		return (String) this.mAttributes.get("status");
	}
	
	public void setLocked(String pLocked) {
		this.mAttributes.put("locked", pLocked);
		
	}
	public String getLocked() {
		return (String) this.mAttributes.get("locked");
	}
	public void setManualLock(String pLocked) {
		this.mAttributes.put("manualLock", pLocked);
		
	}
	public String getManualLock() {
		return (String) this.mAttributes.get("manualLock");
	}
	
	public void setAcctClaimed(String pLocked) {
		this.mAttributes.put("acctClaimed", pLocked);
		
	}
	public String getAcctClaimed() {
		return (String) this.mAttributes.get("acctClaimed");
	}
	
	public void setTerms(String terms) {
		this.mAttributes.put("terms", terms);
		
	}
	public String getTerms() {
		return (String) this.mAttributes.get("terms");
	}
	 public void validateEnterTerms(ValidationContext context) {
	        MessageContext messages = context.getMessageContext();
	        UserAccount oimUser =null;
	        System.out.println("Inside validate Terms inside UserAccount DOB"+this.getDOB());
	        System.out.println("Inside validate Terms inside UserAccount terms"+this.getTerms());
	   /*
	        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss z");
	        BaseSelfServiceUtilities  selfUtilities = null;
			OIMUserServicesClient oimusersrvclient = new OIMUserServicesClient("http://oialcdcdvm001.nyumc.org:14000/OIMRESTServices", "SelfService", "nyuL4ng0n3", "xelsysadm",  selfUtilities);
			SimpleDateFormat sdf =null;
			try{
			sdf   = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
			System.out.println("sdf format 1"+sdf.format(new Date(this.getDOB())));
			//System.out.println("sdf format 2"+sdf.format(this.getDOB()));
			oimUser = oimusersrvclient.searchForOIMUserforAccountClaiming("userName=="+this.getUserName()+";dateOfBirth=="+sdf.format(new Date(this.getDOB())));
	        	//UserAccount oimUser = oimusersrvclient.searchForOIMUserforAccountClaiming("userName=="+this.getUserName());
	        System.out.println("User Account After searching is"+oimUser);
			} catch (UserNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/

	        if (this.getTerms()==null) {
	            messages.addMessage(
	                new MessageBuilder().error().source("terms").defaultText("Please check terms and conditions to continue").build());
	        }
	        
	        if (this.getDOB().equals("")) {
	        	//(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[012])/((19|20)\\d\\d)
	        	
	        

	            messages.addMessage(
	                new MessageBuilder().error().source("terms").defaultText("Please enter the date of birth in mm/dd/yyyy to continue").build());
	        }
			 if(this.getDOB()!=null)
			 {
				   System.out.println("Inside Check DOB format in mm/dd/yyyy");
					String expression = "^(1[0-2]|0?[1-9])/(3[01]|[12][0-9]|0?[1-9])/(?:[0-9]{2})?[0-9]{2}$";
					
					
					Pattern pattern = Pattern.compile(expression,Pattern.CASE_INSENSITIVE);
					Matcher matcher = pattern.matcher(this.getDOB());  
			if(matcher.matches()){  
				System.out.println("Valid DOB in mm/dd/yyyy format");
			}  
			else{
				messages.addMessage(
			         new MessageBuilder().error().source("terms").defaultText("Please enter the date of birth in mm/dd/yyyy to continue").build());
			}
 	 
 	 
 }

	        
	        
	         
	         
	         
	        
	    }
}
