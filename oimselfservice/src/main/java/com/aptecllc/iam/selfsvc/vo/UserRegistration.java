package com.aptecllc.iam.selfsvc.vo;

//~--- non-JDK imports --------------------------------------------------------

import org.springframework.binding.message.MessageBuilder;
import org.springframework.binding.message.MessageContext;
import org.springframework.binding.validation.ValidationContext;
import org.springframework.format.annotation.DateTimeFormat;

//~--- JDK imports ------------------------------------------------------------

import java.io.Serializable;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Date;

/**
 * Class description
 *
 *
 * @version        1.0, 13/11/08
 * @author         Eric A. Fisher, APTEC LLC
 */
public class UserRegistration implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String            LookupKey;
    private String            SSN;
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    private Date              DOB;
    private String            dobMonth;
    private String            dobDay;
    private String            dobYear;
    private String            firstName;
    private String            middleName;
    private String            lastName;
    private String            terms;

    /**
     * Constructs ...
     *
     */
    public UserRegistration() {
        this.LookupKey = "";
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getLookupKey() {
        return LookupKey;
    }

    /**
     * Method description
     *
     *
     * @param lookupKey
     */
    public void setLookupKey(String lookupKey) {
        LookupKey = lookupKey;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getSSN() {
        return SSN;
    }

    /**
     * Method description
     *
     *
     * @param sSN
     */
    public void setSSN(String sSN) {
        SSN = sSN;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public Date getDOB() {
        return DOB;
    }

    /**
     * Method description
     *
     *
     * @param dOB
     */
    public void setDOB(Date dOB) {
        DOB = dOB;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getDobMonth() {
        return dobMonth;
    }

    /**
     * Method description
     *
     *
     * @param dobMonth
     */
    public void setDobMonth(String dobMonth) {
        this.dobMonth = dobMonth;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getDobDay() {
        return dobDay;
    }

    /**
     * Method description
     *
     *
     * @param dobDay
     */
    public void setDobDay(String dobDay) {
        this.dobDay = dobDay;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getDobYear() {
        return dobYear;
    }

    /**
     * Method description
     *
     *
     * @param dobYear
     */
    public void setDobYear(String dobYear) {
        this.dobYear = dobYear;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Method description
     *
     *
     * @param firstName
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    
    public String getTerms() {
        return terms;
    }

    /**
     * Method description
     *
     *
     * @param terms
     */
    public void setTerms(String terms) {
        this.terms = terms;
    }

    
    /**
     * Method description
     *
     *
     * @return
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Method description
     *
     *
     * @param middleName
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Method description
     *
     *
     * @param lastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Method description
     *
     *
     * @param context
     */
    public void validateEnterRegInfo(ValidationContext context) {
        MessageContext messages = context.getMessageContext();

        if (this.getFirstName().isEmpty()) {
            messages.addMessage(
                new MessageBuilder().error().source("FirstName").defaultText("Enter your first name.").build());
        }

        if (this.getLastName().isEmpty()) {
            messages.addMessage(
                new MessageBuilder().error().source("LastName").defaultText("Enter your last name.").build());
        }

        checkDateOfBirth(messages);
    }

 //
    
    public void validateEnterTerms(ValidationContext context) {
        MessageContext messages = context.getMessageContext();
        System.out.println("Inside validate Terms"+this.getTerms());
        if (this.getTerms()==null) {
            messages.addMessage(
                new MessageBuilder().error().source("terms").defaultText("Please check terms and conditions to continue").build());
        }

        /*
         System.out.println("uss.getTerms"+uss.getTerms());
       
        if(uss.getTerms()==null){
        //if (uss.getTerms()!=null || uss.getTerms().isEmpty()) {
            messages.addMessage(new MessageBuilder().error().source("terms").
                defaultText("Please check the terms and conditions to continue.").build());
       
    } 
         
         */
        
    }

//    
    private void checkDateOfBirth(MessageContext messages) {
        boolean          validateDate = true;
        SimpleDateFormat dobFormat    = new SimpleDateFormat("yyyyMMd");

        if (this.getDobMonth().isEmpty() || this.getDobMonth().equals("Month")) {
            messages.addMessage(
                new MessageBuilder().error().source("dobMonth").defaultText("Select your birth month.").build());
            validateDate = false;
        }

        if (this.getDobDay().isEmpty()) {
            messages.addMessage(
                new MessageBuilder().error().source("dobDay").defaultText("Enter your birth day.").build());
            validateDate = false;
        }

        if (this.getDobYear().isEmpty()) {
            messages.addMessage(
                new MessageBuilder().error().source("dobYear").defaultText("Enter your birth year.").build());
            validateDate = false;
        }

        if (validateDate) {
            String dob = this.getDobYear() + this.getDobMonth() + this.getDobDay();

            // System.out.println(dob);
            try {
                Date dobDate = dobFormat.parse(dob);

                this.setDOB(dobDate);

                // System.out.println(dobDate);
            } catch (ParseException e) {
                messages.addMessage(
                    new MessageBuilder().error().source("DOB").defaultText("Date of Birth is Invalid.").build());
            }
        }
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String buildSearchQuery() {
        StringBuilder    query = new StringBuilder();
        SimpleDateFormat sdf   = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");

        query.append("firstName==").append(this.getFirstName()).append(";lastName==").append(this.getLastName()).append(
            ";dateOfBirth==");
        query.append(sdf.format(this.getDOB()));

        return query.toString();
    }
}
